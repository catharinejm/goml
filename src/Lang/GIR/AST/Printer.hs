{-# LANGUAGE UndecidableInstances #-}

module Lang.GIR.AST.Printer where

import Control.Monad.IO.Class
import Control.Monad.State.Strict
import Data.List
import Data.List.Split (splitOn)
import Data.Monoid
import Data.String.Util
import Lang.GIR.AST.Util

data PrettyState = PS { psMargin :: Int
                      , psColumn :: Int
                      , psOutput :: [String]
                      }

type (PrettyM m) = (MonadState PrettyState m)

instance {-# OVERLAPPABLE #-} (Pretty a) => Stringable a where
  tos = pp

class Pretty a where
  prt :: (PrettyM m) => a -> m ()

instance Pretty String where
  prt = pwrite

wrt :: (PrettyM m) => String -> m ()
wrt l = modify $ \s -> s { psColumn = length l + psColumn s
                         , psOutput = l : psOutput s
                         }

pwrite :: (PrettyM m) => String -> m ()
pwrite str = do
  case splitOn "\n" str of
   [] -> return () -- should be impossible anyway
   ls -> mapM_ wrtNL (init ls) >> wrt (last ls)
  where
    wrtNL l = wrt l >> pnewline

pwriteS :: (PrettyM m) => String -> m ()
pwriteS s = pwrite s >> pwrite " "

pwriteNL :: (PrettyM m) => String -> m ()
pwriteNL s = pwrite s >> pnewline

pnewline :: (PrettyM m) => m ()
pnewline = do
  margin <- gets psMargin
  wrt "\n"
  modify $ \s -> s { psColumn = 0 }
  wrt $ take margin (repeat ' ')

playout :: (PrettyM m) => m a -> m a
playout action = do
  oldmargin <- gets psMargin
  modify $ \s -> s { psMargin = psColumn s }
  res <- action
  modify $ \s -> s { psMargin = oldmargin }
  return res

playoutI :: (PrettyM m) => Int -> m a -> m a
playoutI i action = pwrite (take i (repeat ' ')) >> playout action

pgroup :: (PrettyM m, Pretty a) => String -> String -> String -> [a] -> m ()
pgroup start end delim as = do
  pwrite start
  pdelim delim as
  pwrite end

pgroup' :: (PrettyM m, Pretty a) => String -> String -> String -> [a] -> m ()
pgroup' _ _ _ [] = return ()
pgroup' start end delim as = pgroup start end delim as

pdelim :: (PrettyM m, Pretty a) => String -> [a] -> m ()
pdelim _ [] = return ()
pdelim delim as = do
  mapM_ prtD (init as)
  prt (last as)
  where
    prtD a = prt a >> pwrite delim

playoutGroup :: (PrettyM m, Pretty a) => String -> String -> String -> [a] -> m ()
playoutGroup start end _ [] = pwrite start >> pwrite end
playoutGroup start end delim (a:as) = playout $ do
  pwriteS start
  prt a
  pnewline
  mapM_ writeNext as
  pwrite end
  where
    writeNext a = do pwriteS delim
                     prt a
                     pnewline
  
pp :: (Pretty a) => a -> String
pp a = concat . reverse . psOutput $ execState (prt a) (PS 0 0 [])

pprint :: (MonadIO m, Pretty a) => a -> m ()
pprint = liftIO . putStrLn . pp
