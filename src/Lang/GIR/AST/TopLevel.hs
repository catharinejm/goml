module Lang.GIR.AST.TopLevel where

import Data.List
import Data.Map.Strict (Map)
import Data.Monoid
import Data.String.Util
import Lang.GIR.AST.Expr
import Lang.GIR.AST.Printer
import Lang.GIR.AST.Util

data Package = Package { packageName     :: String
                       , packageTypeDefs :: Map String TypeDef
                       , packageDefs     :: Map String Def
                       }
             deriving (Show, Eq)

instance Pretty Package where
  prt (Package name tds defs) = do
    pwriteNL $+ "package" +|+ name
    pnewline
    mapM_ print2nl tds
    mapM_ print2nl defs
    where
      print2nl x = prt x >> pnewline >> pnewline

data TopLevel = TLDef Def
              | TLTypeDef TypeDef
              deriving (Show, Eq)

data Def = Def String [String] BaseType (Maybe Expr)
         deriving (Show, Eq)

instance Named Def where
  nameOf (Def name _ _ _) = name

instance HasSchema Def where
  typeSchema (Def _ tvs typ _) = TypeSchema tvs typ

instance Pretty Def where
  prt (Def name targs typ body) = do
    pwrite $+ "let" +|+ name
    pgroup' "[" "]" "," targs
    pwrite " : "
    prt typ
    maybe (return ()) printBody body
    where
      printBody body = do
        pwrite " ="
        pnewline
        playoutI 2 (prt body)

data TypeDef = TypeDef String [String] BaseType
             deriving (Show, Eq)

instance Named TypeDef where
  nameOf (TypeDef name _ _) = name

instance HasSchema TypeDef where
  typeSchema (TypeDef _ tvs typ) = TypeSchema tvs typ

instance Pretty TypeDef where
  prt (TypeDef name targs typ) = do
    pwrite $+ "type" +|+ name
    pgroup' "[" "]" "," targs
    pwrite " ="
    pnewline
    playoutI 2 (prt typ)
