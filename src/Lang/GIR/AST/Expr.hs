module Lang.GIR.AST.Expr where

import Control.Monad
import Control.Monad.IO.Class
import Data.List
import Data.Monoid
import Data.String.Util
import Lang.GIR.AST.Printer
import Lang.GIR.AST.Util

data CodeSeg = CodeSeg ScopeID [Statement] BaseType
             deriving (Show, Eq)

returnValue :: CodeSeg -> Expr
returnValue (CodeSeg _ [] _) = Lit UnitLit UnitType
returnValue (CodeSeg _ stmts _) = case last stmts of
                                   NakedEx ex -> ex
                                   _ -> Lit UnitLit UnitType

sideEffectingStatements :: CodeSeg -> [Statement]
sideEffectingStatements (CodeSeg _ [] _) = []
sideEffectingStatements (CodeSeg _ sts _) = init sts

instance Pretty CodeSeg where
  prt (CodeSeg _ stmts _) = pdelim "\n" stmts

instance HasType CodeSeg where
  underlyingType (CodeSeg _ _ typ) = typ

data Statement = NakedEx Expr
               -- | Return ScopeID Expr
               | InitVar String BaseType ScopeID Expr
               | SetVar String ScopeID Expr
               deriving (Show, Eq)

instance HasType Statement where
  underlyingType (NakedEx ex) = underlyingType ex
  -- underlyingType (Return _ ex) = underlyingType ex
  underlyingType (InitVar _ _ _ _) = UnitType
  underlyingType (SetVar _ _ _) = UnitType

instance Pretty Statement where
  prt (NakedEx ex) = prt ex
  prt (InitVar var typ sid value) = do
    pwriteS $+ "var" +|+ var +:+ "{" +:+ sid +:+ "}" +|+ ":"
    prt typ
    pwrite " = "
    prt value
  prt (SetVar var sid value) = (pwriteS $+ var +:+ "{" +:+ sid +:+ "}" +|+ "=") >> prt value

data Binding = FunParam Int LValue
             | Var LValue (Maybe Expr)
             deriving (Show, Eq)

boundValue :: Binding  -> Maybe Expr
boundValue (FunParam _ _) = Nothing
boundValue (Var _ mexpr) = mexpr

instance Named Binding where
  nameOf (FunParam _ lv) = nameOf lv
  nameOf (Var lv _) = nameOf lv

instance Pretty Binding where
  prt (FunParam _ lv) = prt lv
  prt (Var lv _) = prt lv

instance HasType Binding where
  underlyingType (FunParam _ lv) = underlyingType lv
  underlyingType (Var lv _) = underlyingType lv

data Expr = LocalName String ScopeID BaseType
          | GlobalName String [BaseType] BaseType
          | App Expr [Expr] BaseType
          | Prop Expr String BaseType
          | Fn [LValue] BaseType ScopeID Expr BaseType
          | Let [(LValue, Expr)] ScopeID Expr BaseType
          | Block CodeSeg BaseType
          | Lit Literal BaseType
          deriving (Show, Eq)

instance HasType Expr where
  underlyingType (LocalName _ _ typ) = typ
  underlyingType (GlobalName _ _ typ) = typ
  underlyingType (App _ _ typ) = typ
  underlyingType (Prop _ _ typ) = typ
  underlyingType (Fn _ _ _ _ typ) = typ
  underlyingType (Let _ _ _ typ) = typ
  underlyingType (Block _ typ) = typ
  underlyingType (Lit _ typ) = typ

instance Pretty Expr where
  prt (LocalName name sid _) = pwrite $+ name +:+ "{" +:+ sid +:+ "}"
  prt (GlobalName name targs _) = pwrite name >> pgroup' "[" "]" "," targs
  prt (App fn args _) = prt fn >> pgroup "(" ")" ", " args
  prt (Prop recv field _) = prt recv >> (pwrite $+ "." +:+ field)
  prt (Fn args retType sid body _) = do
    pwrite $ "fn{" ++ tos sid ++ "}"
    pgroup "(" ")" ", " args
    when (retType /= UnitType) $ do
      pwrite " -> "
      prt retType
    pwriteNL " {"
    printBody
    pwrite "}"
    where
      printBody = case body of
                   (Block seg _) -> do
                     playoutI 2 (prt seg)
                     pnewline
                   _ -> playoutI 2 (prt body) >> pnewline
  prt (Let binds sid body _) = do
    case binds of
     [] -> pwrite ("let{" ++ tos sid ++ "} in") >> playout (prt body)
     ((lv,e):bs) -> do
       printBind ("let{" ++ tos sid ++ "}") lv e
       mapM_ (uncurry (printBind "and")) bs
       pwriteS "in"
       playout (prt body)
    where
      printBind s lv e = do
        pwriteS s
        prt lv
        pwrite " = "
        playout (prt e)
        pnewline
  prt (Block seg@(CodeSeg _ stmts _) _) = do
    case stmts of
     [x] -> do pwriteS "{"
               prt x
               pwrite " }"
     _ -> do pwrite "{"
             playoutI 2 (prt seg)
             pnewline
             pwrite "}"
  prt (Lit lit _) = pwrite $ tos lit

withType :: Expr -> BaseType -> Expr
withType (LocalName name sid _) = LocalName name sid
withType (GlobalName name targs _) = GlobalName name targs
withType (App fn args _) = App fn args
withType (Prop recv field _) = Prop recv field
withType (Fn params rtyp sid body _) = Fn params rtyp sid body
withType (Let binds sid body _) = Let binds sid body
withType (Lit lit _) = Lit lit

unitExpr :: Expr
unitExpr = Lit UnitLit UnitType

newtype ScopeID = ScopeID Int
                deriving (Show, Eq, Ord, Num, Real, Enum, Integral)

instance Stringable ScopeID where
  tos (ScopeID i) = show i

data Scope = Scope { scopeID    :: ScopeID
                   , scopeBinds :: [Binding]
                   }
           deriving (Show, Eq)

data LValue = LValue String BaseType
            deriving (Show, Eq, Ord)

instance HasType LValue where
  underlyingType (LValue _ typ) = typ

instance Pretty LValue where
  prt (LValue name typ) = (pwriteS $+ name +|+ ":") >> prt typ

instance Named LValue where
  nameOf (LValue name _) = name

data TypeSchema = TypeSchema [String] BaseType
                deriving (Show, Eq)

instance HasType TypeSchema where
  underlyingType (TypeSchema _ typ) = typ

data BaseType = TypeName String [BaseType]
              | TypeParam String
              | FnType [BaseType] BaseType
              | Prim String
              | UnitType
              | StructType [LValue]
              | TypeVar Int
              deriving (Show, Eq, Ord)

instance HasType BaseType where
  underlyingType = id

instance Pretty BaseType where
  prt (TypeName name types) = do
    pwrite name
    pgroup' "[" "]" "," types
  prt (TypeParam n) = pwrite $+ "?" +:+ n
  prt (FnType types typ) =
    case types of
     [] -> pwriteS "() ->" >> prt typ
     _ -> pgroup "(" ")" " -> " (types ++ [typ])
  prt (Prim prim) = pwrite prim
  prt UnitType = pwrite "()"
  prt (StructType fields) = do
    pwrite "struct"
    playoutGroup "{" "}" "," fields
  prt (TypeVar _) = pwrite "?"

isConcrete :: BaseType -> Bool
isConcrete (TypeName _ typs) = all isConcrete typs
isConcrete (TypeParam _) = True
isConcrete (FnType params ret) = all isConcrete params && isConcrete ret
isConcrete (Prim _) = True
isConcrete UnitType = True
isConcrete (StructType fields) = all (isConcrete . underlyingType) fields
isConcrete (TypeVar _) = False

isUnit :: BaseType -> Bool
isUnit UnitType = True
isUnit _ = False

newTypeVar :: (MonadIO m) => m BaseType
newTypeVar = TypeVar <$> nextId

data TypeConstraint = HasField String BaseType

class HasType a where
  underlyingType :: a -> BaseType

class HasSchema a where
  typeSchema :: a -> TypeSchema

-- instance {-# OVERLAPABLE #-} (HasSchema a) => HasType a where
--   underlyingType = underlyingType . typeSchema

litType :: Literal -> BaseType
litType (IntLit _) = TypeName "int" []
litType (FloatLit _) = TypeName "float64" []
litType (StringLit _) = TypeName "string" []
litType (CharLit _) = TypeName "rune" []
litType UnitLit = UnitType
