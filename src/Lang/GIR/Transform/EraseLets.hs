module Lang.GIR.Transform.EraseLets where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type EraseM m = TransM ELState m

data ELState = ELState
             deriving (Show, Eq)

emptyELState :: ELState
emptyELState = ELState

runEraseLets :: (MonadIO m) => Package -> m (Either String Package)
runEraseLets package = transformPackage eraseLetsTrans package emptyELState
  where
    eraseLetsTrans = noopTransformers { tnfTransformExpr = eraseLetsExpr
                                      }

eraseLetsExpr :: (EraseM m) => Expr -> m (TResult Expr)
eraseLetsExpr (Let binds sid body typ) = do
  binds <- transform binds
  body <- withNewScope (letBindScope sid binds) $ transform body
  let initVars = map initVar binds
      stmts = initVars ++ [NakedEx body]
  newSID <- ScopeID <$> nextId
  stmts <- rescope sid newSID stmts
  let block = Block (CodeSeg newSID stmts typ) typ
  liftIO $ putStrLn $+ "rescoped block:" +|+ block
  return $ TRUpdate block
  where
    initVar (LValue "_" _, value) = NakedEx value
    initVar (LValue name typ, value) = InitVar name typ sid value
eraseLetsExpr _ = return TRPassThru
