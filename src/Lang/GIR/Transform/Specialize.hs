module Lang.GIR.Transform.Specialize where

import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

data SState = SState { ssSpecializedFns :: M.Map (String, [BaseType]) Def }
emptySState :: SState
emptySState = SState M.empty

type SpecialM m = TransM SState m

runSpecialize :: (MonadIO m) => Package -> m (Either String Package)
runSpecialize package = runExceptT $ do
  (env, st) <- assertSuccess $ runTransform (TEnv specializeTrans package) (emptyTState emptySState)
  let pkg = tePackage env
  return $ pkg { packageDefs = M.union (packageDefs pkg) (namedMap $ M.elems (ssSpecializedFns $ tsUserState st)) }
  where
    specializeTrans = noopTransformers { tnfTransformDef = specializeDef
                                       , tnfTransformExpr = specializeExpr
                                       }
    assertSuccess action = do
      res <- action
      case res of
       Left err -> throwError err
       Right res -> return res

specializeDef :: (SpecialM m) => Def -> m (TResult Def)
specializeDef (Def _ (_:_) _ (Just _)) = return TRCut
specializeDef _ = return TRPassThru

specializeExpr :: (SpecialM m) => Expr -> m (TResult Expr)
specializeExpr (GlobalName name targs@(_:_) _) = do
  (newName, newType) <- specialize name targs
  return . TRUpdate $ GlobalName newName [] newType
-- specializeExpr (LocalName name sid typ) = do
--   mmap <- getsU ((M.!? sid) . ssSIDMap)
--   case mmap of
--    Nothing -> return TRPassThru
--    Just sid -> return . TRUpdate $ LocalName name sid typ
specializeExpr _ = return TRPassThru

lookupSpecializedDef :: (SpecialM m) => String -> [BaseType] -> m (Maybe Def)
lookupSpecializedDef name targs = getsU ((M.!? (name, targs)) . ssSpecializedFns)

specialize :: (SpecialM m) => String -> [BaseType] -> m (String, BaseType)
specialize name targs = do
  mspec <- lookupSpecializedDef name targs
  case mspec of
   Just (Def name _ typ _) -> return (name, typ)
   Nothing -> do
     def@(Def _ tvs _ mbody) <- lookupDef name
     let noBody = throwError $+ "Generic defintion has no body:" +|+ def
     body <- maybe noBody return mbody
     newBody@(Fn _ _ oldSID _ _) <- subst tvs targs body
     newSID <- ScopeID <$> nextId
     newBody <- rescope oldSID newSID newBody
     newBody <- transform newBody
     spcName <- ((name ++ "_specialized") ++) . show <$> nextId
     let newType = underlyingType newBody
     liftIO $ putStrLn $+ "newBody" +|+ newBody
     liftIO $ putStrLn $+ "newType" +|+ newType
     let newDef = Def spcName [] newType (Just newBody)
     modifyU $ \u -> u { ssSpecializedFns = M.insert (name, targs) newDef (ssSpecializedFns u) }
     return (spcName, newType)
  -- where
  --   rescope (Fn params retTyp oldSID body fnTyp) = do
  --     newSID <- ScopeID <$> nextId
  --     modifyU $ \u -> u { ssSIDMap = M.insert oldSID newSID (ssSIDMap u) }
  --     body <- transform body
  --     Fn params retTyp newSID <$> transform body <*> pure fnTyp
