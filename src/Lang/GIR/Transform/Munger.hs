module Lang.GIR.Transform.Munger where

import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Data.Char
import           Data.List
import qualified Data.Map.Strict as M
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

symWord :: (TransM u m) => Char -> m String
symWord ':' = return "_COLON_"
symWord '!' = return "_BANG_"
symWord '#' = return "_HASH_"
symWord '$' = return "_DOLLAR_"
symWord '%' = return "_PCT_"
symWord '&' = return "_AMP_"
symWord '*' = return "_STAR_"
symWord '+' = return "_PLUS_"
symWord '.' = return "_DOT_"
symWord '/' = return "_SLASH_"
symWord '<' = return "_LT_"
symWord '=' = return "_EQ_"
symWord '>' = return "_GT_"
symWord '?' = return "_QMARK_"
symWord '@' = return "_AT_"
symWord '\\' = return "_WHACK_"
symWord '^' = return "_CARET_"
symWord '|' = return "_PIPE_"
symWord '-' = return "_MINUS_"
symWord '~' = return "_TILDE_"
symWord '\'' = return "_PRIME_"
symWord c | isAlphaNum c || c == '_' || isSpace c = return $ c:""
symWord c = throwError $ "no rename defined for char: " ++ show c

munge :: (TransM u m) => String -> m String
munge sym = concat <$> mapM symWord sym

munger :: Transformers ()
munger = noopTransformers { tnfTransformString = fmap TRUpdate . munge }

runMunger :: (MonadIO m) => Package -> m (Either String Package)
runMunger pkg = transformPackage munger pkg ()
