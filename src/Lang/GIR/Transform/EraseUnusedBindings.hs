module Lang.GIR.Transform.EraseUnusedBindings where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.Either
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import qualified Data.Set as S
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type EraseM m = TransM EUState m

data EUState = EUState { euUsedLocals :: S.Set (String, ScopeID) }
             deriving (Show, Eq)

emptyEUState :: EUState
emptyEUState = EUState S.empty

runEraseUnusedBindings :: (MonadIO m) => Package -> m (Either String Package)
runEraseUnusedBindings package = runExceptT $ do
  (env, st) <- join $ assertSuccess <$> runTransform (TEnv traceUsageTrans package) (emptyTState emptyEUState)
  (env, _) <- join $ assertSuccess <$> runTransform (env { teTransformers = eraseUnusedTrans }) st
  return $ tePackage env
  where
    traceUsageTrans = noopTransformers { tnfTransformExpr = traceUsageExpr }
    eraseUnusedTrans = noopTransformers { tnfTransformExpr = eraseUnusedExpr
                                        , tnfTransformStatement = eraseUnusedStatement
                                        }
    assertSuccess (Left err) = throwError err
    assertSuccess (Right res) = return res

traceUsageExpr :: (EraseM m) => Expr -> m (TResult Expr)
traceUsageExpr (LocalName name sid _) = do
  modifyU $ \u -> u { euUsedLocals = S.insert (name, sid) (euUsedLocals u) }
  return TRPassThru
traceUsageExpr _ = return TRPassThru

isUnusedBind :: (EraseM m) => String -> ScopeID -> m Bool
isUnusedBind name sid = getsU (S.notMember (name, sid) . euUsedLocals)

eraseUnusedExpr :: (EraseM m) => Expr -> m (TResult Expr)
eraseUnusedExpr (Let binds sid body typ) = do
  binds <- transform binds
  body <- withNewScope (letBindScope sid binds) $ transform body
  newBinds <- mapM filterUnused binds
  return . TRUpdate $ Let (mergeBinds binds newBinds) sid body typ
  where
    filterUnused (LValue name typ, body) = do
      isUnused <- isUnusedBind name sid
      if isUnused
        then return $ Just (LValue "_" typ, body)
        else return Nothing
    mergeBinds binds newBinds = map (uncurry fromMaybe) $ binds `zip` newBinds
eraseUnusedExpr _ = return TRPassThru

eraseUnusedStatement :: (EraseM m) => Statement -> m (TResult Statement)
eraseUnusedStatement (InitVar name typ sid value) = do
  isUnused <- isUnusedBind name sid
  if isUnused
    then TRUpdate . NakedEx <$> transform value
    else return TRPassThru
eraseUnusedStatement (SetVar name sid value) = do
  isUnused <- isUnusedBind name sid
  if isUnused
    then TRUpdate . NakedEx <$> transform value
    else return TRPassThru
eraseUnusedStatement _ = return TRPassThru
