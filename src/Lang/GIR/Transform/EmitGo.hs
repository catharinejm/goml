module Lang.GIR.Transform.EmitGo where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Set as S
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Munger
import           Lang.GIR.Transform.Transformer

type EmitM m = TransM EGState m

data EGState = EGState { egOutput :: [String]
                       , egIsReturn :: Bool
                       , egReturnType :: BaseType
                       }
             deriving (Show, Eq)

emptyEGState :: EGState
emptyEGState = EGState [] False UnitType

runEmitGo :: (MonadIO m) => Package -> m (Either String String)
runEmitGo package = do
  res <- runTransform (TEnv emitGoTrans package) (emptyTState emptyEGState)
  case res of
   Left err -> return $ Left err
   Right (_, tstate) -> return . Right . concat . reverse . egOutput $ tsUserState tstate
  where
    emitGoTrans = noopTransformers { tnfTransformDef = emitDef
                                   -- , tnfTransformExpr = emitExpr
                                   -- , tnfTransformType = emitBaseType
                                   }

write :: (EmitM m) => String -> m ()
write s = modifyU $ \u -> u { egOutput = s : egOutput u }

writeS :: (EmitM m) => String -> m ()
writeS s = write $ s++" "

writeNL :: (EmitM m) => String -> m ()
writeNL s = write $ s++"\n"

writeMunged :: (EmitM m) => String -> m ()
writeMunged = join . fmap write . munge

writeMungedS :: (EmitM m) => String -> m ()
writeMungedS s = writeMunged s >> write " "

writeDelimM :: (EmitM m) => String -> (a -> m ()) -> [a] -> m ()
writeDelimM delim writer [] = return ()
writeDelimM delim writer elems = do
  mapM_ (\e -> writer e >> write delim) (init elems)
  writer (last elems)

emitDef :: (EmitM m) => Def -> m (TResult Def)
emitDef (Def name [] typ (Just val)) = emitTopLevelDef name typ val >> return TRCut
emitDef (Def name [] _ Nothing) = emitTopLevelBuiltin name >> return TRCut
emitDef _ = return TRCut

emitTopLevelBuiltin :: (EmitM m) => String -> m ()
emitTopLevelBuiltin "*" = do
  writeS "func"
  writeMunged "*"
  write $ unlines [ "(x, y int) int {"
                  , "  return x * y"
                  , "}"
                  ]
emitTopLevelBuiltin undef = throwError $+ "no definition for builtin:" +|+ undef

emitTopLevelDef :: (EmitM m) => String -> BaseType -> Expr -> m ()
emitTopLevelDef name _ (Fn params retType sid body _) = do
  writeS "func"
  writeMunged name
  write "("
  writeDelimM "," (emitScopedLValue sid) params
  write ") "
  emitType retType
  write "{\n"
  withReturn retType $ emitExpr body
  write "}\n"
emitTopLevelDef name typ val = do
  writeS $ "var " ++ name
  emitType typ
  write " = "
  emitExpr val
  write "\n"

emitLValue :: (EmitM m) => LValue -> m ()
emitLValue (LValue name typ) = do
  writeMungedS name
  emitType typ

emitScopedLValue :: (EmitM m) => ScopeID -> LValue -> m ()
emitScopedLValue sid (LValue name typ) = emitLValue (LValue (tagName name sid) typ)

pRIMS :: S.Set String
pRIMS = S.fromList [ "bool"
                   , "int"
                   , "int8"
                   , "int16"
                   , "int32"
                   , "int64"
                   , "intptr"
                   , "uint"
                   , "uint8"
                   , "uint16"
                   , "uint32"
                   , "uint64"
                   , "uintptr"
                   , "float32"
                   , "float64"
                   , "string"
                   , "byte"
                   , "rune"                    
                   ]

emitPrimType :: (EmitM m) => String -> m ()
emitPrimType s = if s `S.member` pRIMS
                 then write s
                 else throwError $+ "unrecognized primitive type:" +|+ s

emitType :: (EmitM m) => BaseType -> m ()
emitType (TypeName name _) = writeMunged name
emitType (TypeParam _) = throwError "encountered type param in Go code emission"
emitType (FnType params ret) = do
  let namedParams = names `zip` params
  write "func("
  writeDelimM "," emitParam namedParams
  write ") "
  emitType ret
  where
    names = [l++n | n <- "":map show [(1::Int)..], l <- map (:[]) ['a'..'z']]
    emitParam = emitLValue . uncurry LValue
emitType (Prim p) = emitPrimType p
emitType UnitType = return ()
emitType (StructType fields) = do
  write "struct{"
  writeDelimM ";" emitLValue fields
  write "}"
emitType (TypeVar _) = throwError "encountered type var in Go code emission"

writeReturn :: (EmitM m) => m a -> m a
writeReturn action = do
  whenIsReturn $ do
    retType <- getsU egReturnType
    when (retType /= UnitType) $ writeS "return"
    modifyU $ \u -> u { egIsReturn = False }
  action

withReturn :: (EmitM m) => BaseType -> m a -> m a
withReturn typ action = do
  (oldRet, oldRetTyp) <- getsU (egIsReturn &&& egReturnType)
  modifyU $ \u -> u { egIsReturn = True, egReturnType = typ}
  res <- action
  modifyU $ \u -> u { egIsReturn = oldRet, egReturnType = oldRetTyp }
  return res

withoutReturn :: (EmitM m) => m a -> m a
withoutReturn action = do
  oldRet <- getsU egIsReturn
  modifyU $ \u -> u { egIsReturn = False }
  res <- action
  modifyU $ \u -> u { egIsReturn = oldRet }
  return res

whenIsReturn :: (EmitM m) => m () -> m ()
whenIsReturn action = do
  isRet <- getsU egIsReturn
  when isRet action

tagName :: String -> ScopeID -> String
tagName name sid = tos $ name +:+ "_" +:+ sid

emitExpr :: (EmitM m) => Expr -> m ()
emitExpr (LocalName "_" _ _) = write "_"
emitExpr (LocalName name sid _) = writeReturn $ writeMunged (tagName name sid)
emitExpr (GlobalName name _ _) = writeReturn $ writeMunged name
emitExpr (App fn args _) = writeReturn $ do
  emitExpr fn
  write "("
  writeDelimM "," emitExpr args
  write ")"
emitExpr (Prop recv field _) = writeReturn $ do
  emitExpr recv
  write $ "." ++ field
emitExpr (Fn params retType sid body _) = writeReturn $ do
  write "func("
  writeDelimM "," (emitScopedLValue sid) params
  writeS ")"
  emitType retType
  writeNL " {"
  withReturn retType $ emitExpr body
  write "\n}"
emitExpr (Let binds sid body _) =
  throwError $+ "cannot emit Let expression. Did you run the EraseLets transformation?"
emitExpr (Block seg _) = do
  -- writeNL "{"
  emitSeg seg
  -- write "\n}"
emitExpr (Lit lit _) = case lit of
                        UnitLit -> return ()
                        _ -> write $+ lit

emitSeg :: (EmitM m) => CodeSeg -> m ()
emitSeg (CodeSeg _ [] _) = return ()
emitSeg (CodeSeg _ [stmt] _) = emitStatement stmt
emitSeg (CodeSeg _ stmts _) = do
  withoutReturn $ writeDelimM "\n" emitStatement (init stmts)
  write "\n"
  emitStatement (last stmts)

emitStatement :: (EmitM m) => Statement -> m ()
emitStatement (NakedEx ex) = do
  emitExpr ex
-- emitStatement (InitVar "_" _ _ value) = emitExpr value
emitStatement (InitVar name typ sid value) = do
  writeMungedS $+ "var" +|+ tagName name sid
  emitType typ
  write " = "
  emitExpr value
  -- whenIsReturn $ do
  --   write "\n"
  --   write "return"
emitStatement (SetVar name sid value) = do
  writeMungedS $+ tagName name sid +|+ "="
  emitExpr value
  -- whenIsReturn $ do
  --   write "\n"
  --   write "return"
