module Lang.GIR.Transform.ScopeNames where

import           Control.Monad.Except
import           Control.Monad.IO.Class
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

data SNState = SNState { snTypeVars :: [String] }
emptySNState :: SNState
emptySNState = SNState []

type ScoperM m = TransM SNState m

runScoper :: (MonadIO m) => Package -> m (Either String Package)
runScoper package = transformPackage scoperTrans package emptySNState
  where
    scoperTrans = noopTransformers { tnfTransformDef = pushTypeVars
                                   , tnfTransformExpr = scopeExpr
                                   , tnfTransformScopeID = genScopeID
                                   , tnfTransformBaseType = rewriteParams
                                   }

tryLookupScopeID :: (ScoperM m) => String -> m (Maybe ScopeID)
tryLookupScopeID name = do
  scopes <- gets tsScopePath
  foldr findName (return Nothing) scopes
  where
    findName (Scope sid binds) ~next = case find ((==name) . nameOf) binds of
                                        Nothing -> next
                                        Just _ -> return $ Just sid

lookupScopeID :: (ScoperM m) => String -> m ScopeID
lookupScopeID name = join $ maybe undefinedName return <$> tryLookupScopeID name
  where
    undefinedName = throwError $+ "undefined local name:" +|+ name

scopeExpr :: (ScoperM m) => Expr -> m (TResult Expr)
scopeExpr (GlobalName name targs typ) = do
  msid <- tryLookupScopeID name
  case msid of
   Nothing -> do
     targs <- transform targs
     TypeSchema tvs _ <- typeSchema <$> lookupDef name
     targs <- if length targs < length tvs
              then (targs ++) <$> replicateM (length tvs - length targs) newTypeVar
              else return targs
     TRUpdate . GlobalName name targs <$> transform typ
   Just sid -> do
     assertTypeArity [] targs
     TRUpdate . LocalName name sid <$> transform typ
scopeExpr (LocalName name _ typ) = do
  TRUpdate <$> (LocalName name <$> lookupScopeID name <*> pure typ)
scopeExpr _ = return TRPassThru

genScopeID :: (ScoperM m) => ScopeID -> m (TResult ScopeID)
genScopeID (ScopeID (-1)) = TRUpdate . ScopeID <$> nextId
genScopeID _ = return TRPassThru

pushTypeVars :: (ScoperM m) => Def -> m (TResult Def)
pushTypeVars (Def name tvs typ body) = do
  when (Set.size (Set.fromList tvs) /= length tvs) $
    throwError "duplicate type parameter name"
  modifyU $ \u -> u { snTypeVars = tvs }
  return TRPassThru

rewriteParams :: (ScoperM m) => BaseType -> m (TResult BaseType)
rewriteParams (TypeName name targs) = do
  isParam <- getsU (elem name . snTypeVars)
  if isParam
    then if null targs
         then return $ TRUpdate $ TypeParam name
         else throwError $+ "type variables cannot take parameters"
    else return TRPassThru
rewriteParams _ = return TRPassThru
