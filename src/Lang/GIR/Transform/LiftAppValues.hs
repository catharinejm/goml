module Lang.GIR.Transform.LiftAppValues where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.State.Strict
import           Data.Either
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import qualified Data.Set as S
import           Data.String.Util
import           Lang.GIR.AST
import           Lang.GIR.Transform.Transformer

type LiftM m = TransM LAState m

data LAState = LAState
             deriving (Show, Eq)

emptyLAState :: LAState
emptyLAState = LAState

runLiftAppValues :: (MonadIO m) => Package -> m (Either String Package)
runLiftAppValues package = transformPackage liftAppValuesTrans package emptyLAState
  where
    liftAppValuesTrans = noopTransformers { tnfTransformExpr = liftAppValuesExpr
                                          }

genParams :: BaseType -> [LValue]
genParams (FnType ptypes _) = map (uncurry LValue) $ pnames `zip` ptypes
  where
    pnames = map (uncurry (++)) (repeat "param" `zip` map show [0..])

getParamLValues :: (LiftM m) => Expr -> m [LValue]
getParamLValues (Fn params _ _ _ _) = return params
getParamLValues (GlobalName name _ _) = do
  Def _ _ typ mexpr <- lookupDef name
  maybe (return $ genParams typ) getParamLValues mexpr
getParamLValues (LocalName name sid _) = do
  binding <- lookupBinding sid name
  case binding of
   FunParam _ (LValue _ typ) -> return $ genParams typ
   Var (LValue _ typ) Nothing -> return $ genParams typ
   Var _ (Just expr) -> getParamLValues expr
getParamLValues (Let _ _ body _) = getParamLValues body
getParamLValues (Block seg _) = getParamLValues (returnValue seg)
getParamLValues expr = return . genParams $ underlyingType expr

liftAppValuesExpr :: (LiftM m) => Expr -> m (TResult Expr)
liftAppValuesExpr (App fn params typ) = do
  fn <- transform fn
  params <- transform params
  typ <- transform typ
  paramLVs <- getParamLValues fn
  let fnLValue = LValue "fn0" (underlyingType fn)
      binds = (fnLValue : paramLVs) `zip` (fn : params)
  newSID <- ScopeID <$> nextId
  let (newFn : newParams) = map (\(LValue n t) -> LocalName n newSID t) (fnLValue : paramLVs)
  return . TRUpdate $ Let binds newSID (App newFn newParams typ) typ
liftAppValuesExpr _ = return TRPassThru
