{-# LANGUAGE UndecidableInstances #-}

module Lang.GoML.AST where

import Control.Monad.IO.Class
import Data.List
import Data.Maybe
import Data.String.Util
import Lang.GoML.Util
import Text.Parsec (SourcePos)

data Name = Local String
          | Global String [BaseType]
          deriving (Show, Eq)

instance Named Name where
  nameOf (Local n) = n
  nameOf (Global n _) = n

data PosRange = PosRange SourcePos SourcePos
              | PosSingle SourcePos
              | NoRange
              deriving (Show, Eq, Ord)

startPos :: PosRange -> Maybe SourcePos
startPos (PosRange st _) = Just st
startPos (PosSingle st) = Just st
startPos NoRange = Nothing

endPos :: PosRange -> Maybe SourcePos
endPos (PosRange _ end) = Just end
endPos _ = Nothing

mergePos :: PosRange -> PosRange -> PosRange
mergePos l r = case (startPos l, endPos r) of
                (Nothing, _) -> NoRange
                (Just st, Nothing) -> PosSingle st
                (Just st, Just end) -> PosRange st end

data NameExpr = NameExpr EID Name
              deriving  (Show, Eq)

instance HasEID NameExpr where
  eID (NameExpr eid _) = eid

instance Named NameExpr where
  nameOf (NameExpr _ name) = nameOf name

toExpr :: NameExpr -> Expr
toExpr (NameExpr eid name) = Name eid name

data Expr = App EID Expr [Expr]
          | Lam EID Sig [String] Expr
          | Let EID String Expr BaseType Expr
          | Prop EID Expr String
          -- | Match EID MatchExpr
          | Name EID Name
          | Lit EID Literal
          | Pre EID PreExpr
          deriving (Show, Eq)

instance HasEID Expr where
  eID (App eid _ _) = eid
  eID (Lam eid _ _ _) = eid
  eID (Let eid _ _ _ _) = eid
  eID (Prop eid _ _) = eid
  eID (Name eid _) = eid
  eID (Lit eid _) = eid
  eID (Pre eid _) = eid

intLit :: Integer -> Expr
intLit = intLit' nonEID

intLit' :: EID -> Integer -> Expr
intLit' eid = Lit eid . IntLit

intLitIO :: (MonadIO io) => Integer -> io Expr
intLitIO = (intLit' <$> genEID <*>) . pure

floatLit :: Double -> Expr
floatLit = floatLit' nonEID

floatLit' :: EID -> Double -> Expr
floatLit' eid = Lit eid . FloatLit

floatLitIO :: (MonadIO io) => Double -> io Expr
floatLitIO = (floatLit' <$> genEID <*>) . pure

stringLit :: String -> Expr
stringLit = stringLit' nonEID

stringLit' :: EID -> String -> Expr
stringLit' eid = Lit eid . StringLit

stringLitIO :: (MonadIO io) => String -> io Expr
stringLitIO = (stringLit' <$> genEID <*>) . pure

charLit :: Char -> Expr
charLit = charLit' nonEID

charLit' :: EID -> Char -> Expr
charLit' eid = Lit eid . CharLit

charLitIO :: (MonadIO io) => Char -> io Expr
charLitIO = (charLit' <$> genEID <*>) . pure

unitLit' :: EID -> Expr
unitLit' eid = Lit eid UnitLit

localName :: String -> Expr
localName = localName' nonEID

localName' :: EID -> String -> Expr
localName' eid = Name eid . Local

localNameIO :: (MonadIO io) => String -> io Expr
localNameIO = (localName' <$> genEID <*>) . pure

globalName :: String -> [BaseType] -> Expr
globalName = globalName' nonEID

globalName' :: EID -> String -> [BaseType] -> Expr
globalName' eid name types = Name eid $ Global name types

data PreExpr = PELet String Expr Expr
             | PELam [String] Expr
             | PEOp Expr NameExpr Expr
             | PEName Name
             deriving (Show, Eq)

peLet :: String -> Expr -> Expr -> Expr
peLet = peLet' nonEID

peLet' :: EID -> String -> Expr -> Expr -> Expr
peLet' eid name value body = Pre eid $ PELet name value body

peLetIO :: (MonadIO io) => String -> Expr -> Expr -> io Expr
peLetIO name value body = Pre <$> genEID <*> pure (PELet name value body)

peLam :: [String] -> Expr -> Expr
peLam = peLam' nonEID

peLam' :: EID -> [String] -> Expr -> Expr
peLam' eid args body = Pre eid $ PELam args body

peLamIO :: (MonadIO io) => [String] -> Expr -> io Expr
peLamIO args body = Pre <$> genEID <*> pure (PELam args body)

peOp' :: EID -> Expr -> NameExpr -> Expr -> Expr
peOp' eid lhs op rhs = Pre eid $ PEOp lhs op rhs

data Literal = IntLit Integer
             | FloatLit Double
             | StringLit String
             | CharLit Char
             | UnitLit
             deriving (Show, Eq)

instance Stringable Literal where
  tos (IntLit i) = show i
  tos (FloatLit f) = show f
  tos (StringLit s) = show s
  tos (CharLit c) = show c
  tos UnitLit = "()"

class HasType a where
  underlyingType :: a -> BaseType

instance {-# OVERLAPS #-} (HasSchema a) => HasType a where
  underlyingType = underlyingType . typeSchema

class HasSchema a where
  typeSchema :: a -> TypeSchema

data FunDecl = FunDecl { funEID    :: EID
                       , funName   :: String
                       , funSchema :: TypeSchema
                       , funParams :: [String]
                       , funBody   :: Expr
                       }
             | BuiltinDecl EID String TypeSchema
             | CtorDecl EID String TypeSchema
             deriving (Show, Eq)

instance HasSchema FunDecl where
  typeSchema (FunDecl _ _ schema _ _) = schema
  typeSchema (BuiltinDecl _ _ schema) = schema
  typeSchema (CtorDecl _ _ schema) = schema

instance HasEID FunDecl where
  eID = funEID

instance Named FunDecl where
  nameOf (FunDecl _ name _ _ _) = name
  nameOf (BuiltinDecl _ name _) = name
  nameOf (CtorDecl _ name _) = name

data TypeSchema = TypeSchema [String] BaseType
                deriving (Show, Eq)

instance Stringable TypeSchema where
  tos (TypeSchema [] typ) = tos typ
  tos (TypeSchema vars typ) = "forall " ++ intercalate " " vars ++ ". " ++ tos typ

instance HasType TypeSchema where
  underlyingType (TypeSchema _ typ) = typ

data TypeDef = TypeDef EID String TypeSchema
             deriving (Show, Eq)

instance HasSchema TypeDef where
  typeSchema (TypeDef _ _ schema) = schema

instance HasEID TypeDef where
  eID (TypeDef eid _ _) = eid

instance Named TypeDef where
  nameOf (TypeDef _ name _) = name

data Fixity = InfixR Int String
            | InfixL Int String
            | Infix Int String
            deriving (Show, Eq)

instance Named Fixity where
  nameOf (InfixR _ name) = name
  nameOf (InfixL _ name) = name
  nameOf (Infix  _ name) = name

data Import = Import String String
            deriving (Show, Eq)

instance Named Import where
  nameOf (Import alias _) = alias

newtype PackageName = PackageName String
                    deriving (Show, Eq)

instance Stringable PackageName where
  tos (PackageName name) = name

instance Named PackageName where
  nameOf (PackageName name) = name

data Package = Package PackageName [Import] [TopLevel]
             deriving (Show, Eq)

data TopLevel = TLFun FunDecl
              | TLType TypeDef
              | TLFix Fixity
              deriving (Show, Eq)

-- instance HasEID TopLevel where
--   eID (TLFun fd) = eID fd
--   eID (TLType td) = eID td

instance Named TopLevel where
  nameOf (TLFun fd) = nameOf fd
  nameOf (TLType td) = nameOf td
  nameOf (TLFix tf) = nameOf tf

-- instance HasSchema TopLevel where
--   typeSchema (TLFun fd) = typeSchema fd
--   typeSchema (TLType td) = typeSchema td

emptySchema :: BaseType -> TypeSchema
emptySchema = TypeSchema []

data Sig = Sig [BaseType] BaseType
         deriving (Show, Eq)

data BaseType = TypeVar Int
              | TypeParam String
              | NamedType String [BaseType]
              | FunType Sig
              | Prim Prim
              | UnitType
              | StructType (Maybe String) [String] [BaseType]
              | SliceType BaseType
              deriving (Show, Eq)

allTypeEquiv :: [BaseType] -> [BaseType] -> Bool
allTypeEquiv ltyps rtyps =
  length ltyps == length rtyps && all (uncurry typeEquiv) (ltyps `zip` rtyps)

typeEquiv :: BaseType -> BaseType -> Bool
typeEquiv (TypeVar l) (TypeVar r) = l == r
typeEquiv (TypeParam l) (TypeParam r) = l == r
typeEquiv (NamedType lname ltyps) (NamedType rname rtyps) = lname == rname && allTypeEquiv ltyps rtyps
typeEquiv (FunType (Sig ltyps lret)) (FunType (Sig rtyps rret)) = allTypeEquiv ltyps rtyps && lret `typeEquiv` rret
typeEquiv (Prim l) (Prim r) = l == r
typeEquiv (StructType lname lfields ltyps) (StructType rname rfields rtyps) =
  sameNameOrAnon && lfields == rfields && allTypeEquiv ltyps rtyps
  where
    sameNameOrAnon = case (lname, rname) of
                      (Just l, Just r) -> l == r
                      _ -> True
typeEquiv UnitType UnitType = True
typeEquiv _ _ = False

isStruct :: BaseType -> Bool
isStruct (StructType _ _ _) = True
isStruct _ = False

isVar :: BaseType -> Bool
isVar (TypeVar _) = True
isVar _ = False

isParam :: BaseType -> Bool
isParam (TypeParam _) = True
isParam _ = False

isUnit :: BaseType -> Bool
isUnit UnitType = True
isUnit _ = False

data Constraint = HasField String BaseType
                -- | HasTypeArgs [BaseType]
                deriving (Show, Eq)

instance Stringable Constraint where
  tos (HasField name typ) = "{ ..., " ++ name ++ " : " ++ tos typ ++ ", ... }"
  -- tos (HasTypeArgs args) = "? " ++ intercalate " " (map tos args)

data Prim = BoolT
          | IntT
          | Int8T
          | Int16T
          | Int32T
          | Int64T
          | IntPtrT
          | UIntT
          | UInt8T
          | UInt16T
          | UInt32T
          | UInt64T
          | UIntPtrT
          | Float32T
          | Float64T
          | StringT
          deriving (Show, Eq)

instance Stringable Prim where
  tos BoolT    = "bool"
  tos IntT     = "int"
  tos Int8T    = "int8"
  tos Int16T   = "int16"
  tos Int32T   = "int32"
  tos Int64T   = "int64"
  tos IntPtrT  = "intptr"
  tos UIntT    = "uint"
  tos UInt8T   = "uint8"
  tos UInt16T  = "uint16"
  tos UInt32T  = "uint32"
  tos UInt64T  = "uint64"
  tos UIntPtrT = "uintptr"
  tos Float32T = "float32"
  tos Float64T = "float64"
  tos StringT  = "string"

funArity :: BaseType -> Int
funArity (FunType (Sig args _)) = length args
funArity _ = 0

printInner :: BaseType -> String
printInner (NamedType t ts) = intercalate " " (t : map tos ts)
printInner (FunType (Sig typs typ)) = intercalate " -> " (map tos typs ++ [tos typ])
printInner other = tos other

instance Stringable BaseType where
  tos (TypeVar _) = "?"
  tos (TypeParam t) = t
  tos nt@(NamedType _ _) = "(" ++ printInner nt ++ ")"
  tos fun@(FunType _) = "(" ++ printInner fun ++ ")"
  tos (Prim p) = tos p
  tos UnitType = "()"
  tos (StructType mname fields typs) = fromMaybe "struct" mname ++ "{ " ++
                                       intercalate ", " fieldStrs ++
                                       " }"
    where
      fieldStrs = map (\(f, t) -> f ++ " : " ++ tos t) (fields `zip` typs)
  tos (SliceType typ) = "[]"++tos typ
