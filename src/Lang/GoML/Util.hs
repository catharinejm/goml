module Lang.GoML.Util where

import           Control.Arrow
import           Control.Monad.IO.Class
import           Data.IORef
import qualified Data.Map.Strict as M
import           System.IO.Unsafe

class Named a where
  nameOf :: a -> String

namedPairs :: (Named a) => [a] -> [(String, a)]
namedPairs = map (nameOf &&& id)

namedMap :: (Named a) => [a] -> M.Map String a
namedMap = M.fromList . namedPairs

idRef :: IORef Int
idRef = unsafePerformIO (newIORef 0)

nextId :: (MonadIO io) => io Int
nextId = liftIO $ do id <- readIORef idRef
                     modifyIORef' idRef (+1)
                     return id
newtype EID = EID Int
            deriving (Show, Eq, Ord)

class HasEID a where
  eID :: a -> EID

instance HasEID EID where
  eID = id

genEID :: (MonadIO io) => io EID
genEID = nextId >>= return . EID

nonEID :: EID
nonEID = EID (-1)

toi :: (Integral a, Num b) => a -> b
toi = fromIntegral
