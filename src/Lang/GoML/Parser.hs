module Lang.GoML.Parser where

import           Control.Arrow
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.List.Split (splitOn)
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Util
import           System.IO
import           Text.Parsec
import           Text.Parsec.Language (haskellDef)
import qualified Text.Parsec.Pos as P
import qualified Text.Parsec.Token as P

data PState = PState { psRanges :: M.Map EID PosRange
                     , psStart :: Maybe P.SourcePos
                     , psEnd :: Maybe P.SourcePos
                     }
            deriving (Show)

emptyPState :: PState
emptyPState = PState M.empty Nothing Nothing

type Parser m a = ParsecT String PState m a

oP_LETTERS :: String
oP_LETTERS = ":!#$%&*+./<=>?@\\^|-~"

rESERVED_OPS :: [String]
rESERVED_OPS = [":","..","=","\\","|","<-","->","@","~","=>", "."]

langDef :: (Stream s m Char, MonadIO m) => P.GenLanguageDef s PState m
langDef = P.LanguageDef { P.commentStart = P.commentStart haskellDef
                        , P.commentEnd = P.commentEnd haskellDef
                        , P.commentLine = P.commentLine haskellDef
                        , P.nestedComments = P.nestedComments haskellDef
                        , P.identStart = letter <|> char '_'
                        , P.identLetter = alphaNum <|> oneOf "_'"
                        , P.opStart = P.opLetter langDef
                        , P.opLetter = oneOf oP_LETTERS
                        , P.reservedNames = [ "package", "builtin", "forall", "import"
                                            , "let", "in"
                                            , "infix", "infixl", "infixr"
                                            , "type", "interface" , "struct"
                                            ]
                        , P.reservedOpNames = rESERVED_OPS
                        , P.caseSensitive = True
                        }

endThenWhitespace :: (MonadIO m) => Parser m ()
endThenWhitespace = do
  pos <- getPosition
  modifyP $ \s -> s { psEnd = Just pos }
  P.whiteSpace lexer

lexer :: (MonadIO m) => P.GenTokenParser String PState m
lexer = (P.makeTokenParser langDef) { P.lexeme = \p -> (do res <- p; endThenWhitespace; return res) }

modifyP :: (MonadIO m) => (PState -> PState) -> Parser m ()
modifyP f = do
  st <- getState
  putState (f st)

lookupRange :: (MonadIO m, HasEID e, Show e) => e -> Parser m PosRange
lookupRange expr = do
  st <- getState
  case (psRanges st) M.!? (eID expr) of
   Nothing -> fail $+ "no such EID:" +|+ S expr
   Just range -> return range

posWrap :: (MonadIO m) => Parser m a -> Parser m (a, PosRange)
posWrap p = do
  start <- getPosition
  (oldStart, oldEnd) <- getState >>= return . (psStart &&& psEnd)
  modifyP $ \s -> s { psStart = Just start }
  res <- p
  end <- getState >>= return . psEnd
  let range = case end of
               Nothing -> PosSingle start
               Just e -> PosRange start e
  modifyP $ \s -> s { psStart = oldStart
                    , psEnd = oldEnd
                    }
  return (res, range)

parens :: (MonadIO m) => Parser m a -> Parser m (a, PosRange)
parens = posWrap . lexeme . P.parens lexer

braces :: (MonadIO m) => Parser m a -> Parser m (a, PosRange)
braces = posWrap . lexeme . P.braces lexer

brackets :: (MonadIO m) => Parser m a -> Parser m (a, PosRange)
brackets = posWrap . lexeme . P.brackets lexer

joinPos :: (MonadIO m) => Parser m ((a, PosRange), PosRange) -> Parser m (a, PosRange)
joinPos p = do
  ((res, _), range) <- p
  return (res, range)

symbol :: (MonadIO m) => String -> Parser m (String, PosRange)
symbol = posWrap . P.symbol lexer

lexeme :: (MonadIO m) => Parser m a -> Parser m a
lexeme = P.lexeme lexer

identifier :: (MonadIO m) => Parser m (String, PosRange)
identifier = posWrap (P.identifier lexer)

identifierOp :: (MonadIO m) => Parser m (String, PosRange)
identifierOp = posWrap (between backtick backtick $ P.identifier lexer)

reserved :: (MonadIO m) => String -> Parser m ((), PosRange)
reserved = posWrap . P.reserved lexer

operator :: (MonadIO m) => Parser m (String, PosRange)
operator = posWrap (P.operator lexer)

reservedOp :: (MonadIO m) => String -> Parser m ((), PosRange)
reservedOp = posWrap . P.reservedOp lexer

natural :: (MonadIO m) => Parser m (Integer, PosRange)
natural = posWrap (P.natural lexer)

float :: (MonadIO m) => Parser m (Double, PosRange)
float = posWrap (P.float lexer)

charLiteral :: (MonadIO m) => Parser m (Char, PosRange)
charLiteral = posWrap (P.charLiteral lexer)

stringLiteral :: (MonadIO m) => Parser m (String, PosRange)
stringLiteral = posWrap (P.stringLiteral lexer)

semi :: (MonadIO m) => Parser m (String, PosRange)
semi = posWrap (P.semi lexer)

comma :: (MonadIO m) => Parser m (String, PosRange)
comma = posWrap (P.comma lexer)

colon :: (MonadIO m) => Parser m (String, PosRange)
colon = posWrap (P.colon lexer)

dot :: (MonadIO m) => Parser m (String, PosRange)
dot = reservedOp "." >>= \(_, r) -> return (".", r)

backtick :: (MonadIO m) => Parser m (String, PosRange)
backtick = posWrap $ lexeme (string "`")

commaSep :: (MonadIO m) => Parser m a -> Parser m [a]
commaSep = P.commaSep lexer

semiSep :: (MonadIO m) => Parser m a -> Parser m [a]
semiSep = P.semiSep lexer

statement :: (MonadIO m) => Parser m a -> Parser m a
statement p = do
  res <- p
  void semi
  return res

typeSchemaP :: (MonadIO m) => Parser m TypeSchema
typeSchemaP = try genericType <|> (emptySchema <$> baseType )

baseType :: (MonadIO m) => Parser m BaseType
baseType = fst <$> monoType

replaceVars :: (MonadIO m) => [String] -> BaseType -> Parser m BaseType
replaceVars tvars (NamedType n []) = if n `elem` tvars
                                       then return $ TypeParam n
                                       else return $ NamedType n []
replaceVars tvars (NamedType n typs) = if n `elem` tvars
                                       then fail "type vars cannot accept params"
                                       else NamedType n <$> mapM (replaceVars tvars) typs
replaceVars tvars (FunType (Sig argTyps retType)) =
  FunType <$> (Sig <$> mapM (replaceVars tvars) argTyps <*> replaceVars tvars retType)
replaceVars tvars (StructType sname fields typs) =
  StructType sname fields <$> mapM (replaceVars tvars) typs
replaceVars _ typ = return typ

genericType :: (MonadIO m) => Parser m TypeSchema
genericType = do
  void $ reserved "forall"
  tvars <- many1 identifier >>= return . map fst
  void $ dot
  (typ, _) <- monoType
  TypeSchema tvars <$> replaceVars tvars typ

monoType :: (MonadIO m) => Parser m (BaseType, PosRange)
monoType = do
  segs <- (try typeApp <|> singleType) `sepBy1` reservedOp "->"
  case segs of
   [typeAndRange] -> return typeAndRange
   typesAndRanges -> let (typs, ranges) = unzip typesAndRanges
                     in return (FunType $ Sig (init typs) (last typs), last ranges)

typeApp :: (MonadIO m) => Parser m (BaseType, PosRange)
typeApp = do
  (tname, st) <- identifier
  (typeArgs, ranges) <- unzip <$> many1 singleType
  return (NamedType tname typeArgs, mergePos st (last ranges))

singleType :: (MonadIO m) => Parser m (BaseType, PosRange)
singleType = try unitTypeP
             <|> try (joinPos (parens monoType))
             <|> try sliceTypeP
             <|> try structTypeP
             <|> (identifier >>= \(n, r) -> return (NamedType n [], r))

unitTypeP :: (MonadIO m) => Parser m (BaseType, PosRange)
unitTypeP = do
  (_, st) <- symbol "("
  (_, end) <- symbol ")"
  return (UnitType, mergePos st end)

structTypeP :: (MonadIO m) => Parser m (BaseType, PosRange)
structTypeP = do
  (_, st) <- reserved "struct"
  (fields, end) <- braces $ commaSep field
  return (StructType Nothing (map fst fields) (map snd fields), mergePos st end)
  where
    field = do
      (name, _) <- identifier
      void colon
      (typ, _) <- monoType
      return (name, typ)

sliceTypeP :: (MonadIO m) => Parser m (BaseType, PosRange)
sliceTypeP = do
  (typ, range) <- joinPos (brackets monoType)
  return (SliceType typ, range)

expr :: (MonadIO m) => Parser m Expr
expr = try operatorExpr <|> try appExpr <|> singleExpr

addRange :: (MonadIO m) => PosRange -> PosRange -> Parser m EID
addRange st end = addFullRange (mergePos st end)

addFullRange :: (MonadIO m) => PosRange -> Parser m EID
addFullRange range = do
  eid <- genEID
  modifyP $ \s -> s { psRanges = M.insert eid range (psRanges s) }
  return eid

lambdaExpr :: (MonadIO m) => Parser m Expr
lambdaExpr = do
  (_, st) <- reservedOp "\\"
  args <- many1 identifier >>= return . map fst
  void $ reservedOp "->"
  body <- expr
  eid <- lookupRange body >>= addRange st
  peLam' eid args <$> localizeNames args body

letExpr :: (MonadIO m) => Parser m Expr
letExpr = do
  (_, st) <- reserved "let"
  (name, _) <- identifier
  void $ reservedOp "="
  value <- expr
  void $ reserved "in"
  body <- expr
  eid <- lookupRange body >>= addRange st
  peLet' eid name value <$> localizeNames [name] body

localizeNames :: (MonadIO m) => [String] -> Expr -> Parser m Expr
localizeNames locals (App eid fn args) =
  App eid <$> localizeNames locals fn <*> mapM (localizeNames locals) args
localizeNames locals (Lam eid sig names body) =
  Lam eid sig names <$> localizeNames (names++locals) body
localizeNames locals (Let eid name value typ body) =
  Let eid name <$> localizeNames locals value <*> pure typ <*> localizeNames (name:locals) body
localizeNames locals (Prop eid recv field) =
  Prop eid <$> localizeNames locals recv <*> pure field
localizeNames locals (Pre eid (PELam names body)) =
  peLam' eid names <$> localizeNames (names++locals) body
localizeNames locals (Pre eid (PELet name value body)) =
  peLet' eid name <$> localizeNames locals value <*> localizeNames (name:locals) body
localizeNames locals (Pre eid (PEOp lhs (NameExpr nEID name) rhs)) =
  peOp' eid <$> localizeNames locals lhs
            <*> (NameExpr nEID <$> localize' locals name)
            <*> localizeNames locals rhs
localizeNames locals (Pre eid (PEName name)) =
  Pre eid . PEName <$> localize' locals name
localizeNames locals (Name eid name) =
  Name eid <$> localize' locals name
localizeNames _ expr = return expr

localize' :: (MonadIO m) => [String] -> Name -> Parser m Name
localize' locals (Global name []) = if name `elem` locals
                                    then return $ Local name
                                    else return $ Global name []
localize' locals (Global name tvs) = if name `elem` locals
                                     then fail "local names cannot accept type params"
                                     else return $ Global name tvs
localize' _ local = return local

operatorLiteral :: (MonadIO m) => Parser m NameExpr
operatorLiteral = do
  (op, range) <- operator
  eid <- addFullRange range
  return $ NameExpr eid (Global op [])

operatorExpr :: (MonadIO m) => Parser m Expr
operatorExpr = do
  lhs <- (try appExpr <|> singleExpr)
  op <- operatorLiteral
  rhs <- expr
  eid <- join $ addRange <$> lookupRange lhs <*> lookupRange rhs
  return $ peOp' eid lhs op rhs

appExpr :: (MonadIO m) => Parser m Expr
appExpr = do
  fn <- singleExpr
  args <- many1 singleExpr
  eid <- join $ addRange <$> lookupRange fn <*> lookupRange (last args)
  return $ App eid fn args

singleExpr :: (MonadIO m) => Parser m Expr
singleExpr = try propAccessExpr
             <|> try parenExpr
             <|> try lambdaExpr
             <|> try letExpr
             <|> simpleExpr

simpleExpr :: (MonadIO m) => Parser m Expr
simpleExpr = try unitExpr
             <|> try numberExpr
             <|> try charExpr
             <|> try stringExpr
             <|> nameExpr

unitExpr :: (MonadIO m) => Parser m Expr
unitExpr = do
  (_, st) <- symbol "("
  (_, end) <- symbol ")"
  eid <- addRange st end
  return $ unitLit' eid

propRecv :: (MonadIO m) => Parser m Expr
propRecv = try parenExpr <|> simpleExpr

propRHS :: (MonadIO m) => Parser m (String, PosRange)
propRHS = do
  void $ dot
  identifier

propAccessExpr :: (MonadIO m) => Parser m Expr
propAccessExpr = do
  recv <- propRecv
  chain <- many1 propRHS
  foldM (\recv (fld, end) -> do
            eid <- join $ addRange <$> lookupRange recv <*> pure end
            return $ Prop eid recv fld
        ) recv chain

parenExpr :: (MonadIO m) => Parser m Expr
parenExpr = do
  (expr, range) <- parens expr
  modifyP $ \s -> s { psRanges = M.insert (eID expr) range (psRanges s) }
  return expr

numberExpr :: (MonadIO m) => Parser m Expr
numberExpr = try floatExpr <|> integerExpr

floatExpr :: (MonadIO m) => Parser m Expr
floatExpr = do
  mneg <- optionMaybe (posWrap (char '-'))
  (f, range) <- float
  case mneg of
   Nothing -> do
     eid <- addFullRange range
     return $ floatLit' eid f
   Just (_, st) -> do
     eid <- addRange st range
     return $ floatLit' eid (-f)

integerExpr :: (MonadIO m) => Parser m Expr
integerExpr = do
  mneg <- optionMaybe (posWrap (char '-'))
  (i, range) <- natural
  case mneg of
   Nothing -> do
     eid <- addFullRange range
     return $ intLit' eid i
   Just (_, st) -> do
     eid <- addRange st range
     return $ intLit' eid (-i)

charExpr :: (MonadIO m) => Parser m Expr
charExpr = do
  (c, range) <- charLiteral
  eid <- addFullRange range
  return $ charLit' eid c

stringExpr :: (MonadIO m) => Parser m Expr
stringExpr = do
  (s, range) <- stringLiteral
  eid <- addFullRange range
  return $ stringLit' eid s

nameExpr :: (MonadIO m) => Parser m Expr
nameExpr = do
  (n, range) <- name
  eid <- addFullRange range
  return $ Pre eid $ PEName (Global n [])

name :: (MonadIO m) => Parser m (String, PosRange)
name = try identifier <|> joinPos (parens (try operator <|> identifierOp))

sameName :: (MonadIO m) => String -> Parser m ()
sameName name = void $ try (symbol name) <|> (parens (symbol name) >>= return . fst)

funDecl :: (MonadIO m) => Parser m FunDecl
funDecl = try builtinDecl <|> (statement typeAnn >>= funDef)

builtinDecl :: (MonadIO m) => Parser m FunDecl
builtinDecl = do
  (_, st) <- reserved "builtin"
  (name, schema, range) <- typeAnn
  eid <- addRange st range
  return $ BuiltinDecl eid name schema

typeAnn :: (MonadIO m) => Parser m (String, TypeSchema, PosRange)
typeAnn = do
  (name, st) <- name
  void $ colon
  schema <- typeSchemaP
  return (name, schema, st)

upto :: (MonadIO m, Show a) => Int -> Parser m a -> Parser m [a]
upto n p = aux p (n `max` 0) []
  where
    aux p 0 ary = notFollowedBy p *> pure (reverse ary)
    aux p n ary = (try p >>= aux p (n-1) . (:ary)) <|> return (reverse ary)

exactly :: (MonadIO m, Show a) => Int -> Parser m a -> Parser m [a]
exactly n p = count n p <* notFollowedBy p

funDef :: (MonadIO m) => (String, TypeSchema, PosRange) -> Parser m FunDecl
funDef (name, schema@(TypeSchema _ typ), st) = do
  sameName name
  (args, _) <- (upto (funArity typ) identifier <?> "too many parameters in definition") >>= return . unzip
  void $ reservedOp "="
  body <- expr
  eid <- lookupRange body >>= addRange st
  let schema' = shiftReturn args schema
  FunDecl eid name schema' args <$> localizeNames args body
  where
    shiftReturn defArgs (TypeSchema tvs (FunType (Sig typArgs ret))) =
      TypeSchema tvs (FunType (Sig
                               (take (length defArgs) typArgs)
                               (mkRet (drop (length defArgs) typArgs) ret)))
    shiftReturn _ schema = schema
    mkRet [] ret = ret
    mkRet args ret = FunType (Sig args ret)

typeDef :: (MonadIO m) => Parser m TypeDef
typeDef = do
  (_, st) <- reserved "type"
  (tname, _) <- name
  tvars <- many identifier >>= return . map fst
  void $ reservedOp "="
  (typ, end) <- monoType
  eid <- addRange st end
  TypeDef eid tname . TypeSchema tvars <$> replaceVars tvars (nameStruct tname typ)
  where
    nameStruct tname (StructType _ fields typs) = StructType (Just tname) fields typs
    nameStruct _ typ = typ


fixity :: (MonadIO m) => Parser m Fixity
fixity = try infixLeft <|> try infixRight <|> infixNone
  where
    infixLeft  = pure InfixL <* reserved "infixl" <*> (toi <$> P.integer lexer) <*> P.operator lexer
    infixRight = pure InfixR <* reserved "infixr" <*> (toi <$> P.integer lexer) <*> P.operator lexer
    infixNone  = pure Infix  <* reserved "infix"  <*> (toi <$> P.integer lexer) <*> P.operator lexer

packageNameP :: (MonadIO m) => Parser m PackageName
packageNameP = pure PackageName <* reserved "package" <*> P.identifier lexer

importP :: (MonadIO m) => Parser m Import
importP = do
  void $ reserved "import"
  malias <- optionMaybe identifier
  (path, _) <- stringLiteral
  when (null path) $
    fail "import path must not be empty"
  alias <- case malias of
            Nothing -> do
              let lastPart = last (splitOn "/" path)
              ident <- runParserT (P.identifier lexer <* eof) emptyPState "" lastPart
              case ident of
               Left _ -> fail $ "The last portion of import path, \"" ++ lastPart ++ "\", is not a valid identifier. Please supply an alias."
               Right _ -> return lastPart
            Just (alias, _) -> return alias
  return $ Import alias path

topLevel :: (MonadIO m) => Parser m TopLevel
topLevel = try (TLType <$> typeDef)
           <|> try (TLFix <$> fixity)
           <|> (TLFun <$> funDecl)

packageP :: (MonadIO m) => Parser m Package
packageP = do
  spaces
  pname <- packageNameP
  void semi
  imports <- importP `endBy` semi
  topLevels <- topLevel `endBy` semi
  eof
  return $ Package pname imports topLevels

parseFile :: (MonadIO m) => FilePath -> m (Either ParseError Package)
parseFile file = do
  source <- liftIO $ readFile file
  runParserT packageP emptyPState file source

parseSource :: (MonadIO m) => String -> m (Either ParseError Package)
parseSource = (fmap.fmap) fst . runParse packageP

runParse :: (MonadIO m) => Parser m a -> String -> m (Either ParseError (a, PState))
runParse p = runParserT (do a <- p; st <- getState; return (a, st)) emptyPState ""

parseType :: (MonadIO m) => String -> m (Either ParseError BaseType)
parseType s = runParse baseType s >>= return . fmap fst

parseExpr :: (MonadIO m) => String -> m (Either ParseError Expr)
parseExpr s = runParse expr s >>= return . fmap fst
