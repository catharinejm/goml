module Lang.GoML.Passes.Renamer where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.State.Strict
import           Data.Char
import           Data.List
import qualified Data.Map.Strict as M
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Interregnum
import           Lang.GoML.Passes.Transform

symWord :: (MungerM m) => Char -> m String
symWord ':' = return "_COLON_"
symWord '!' = return "_BANG_"
symWord '#' = return "_HASH_"
symWord '$' = return "_DOLLAR_"
symWord '%' = return "_PCT_"
symWord '&' = return "_AMP_"
symWord '*' = return "_STAR_"
symWord '+' = return "_PLUS_"
symWord '.' = return "_DOT_"
symWord '/' = return "_SLASH_"
symWord '<' = return "_LT_"
symWord '=' = return "_EQ_"
symWord '>' = return "_GT_"
symWord '?' = return "_QMARK_"
symWord '@' = return "_AT_"
symWord '\\' = return "_WHACK_"
symWord '^' = return "_CARET_"
symWord '|' = return "_PIPE_"
symWord '-' = return "_MINUS_"
symWord '~' = return "_TILDE_"
symWord '\'' = return "_PRIME_"
symWord c | isAlphaNum c || c == '_' = return $ c:""
symWord c = throwError $ "no rename defined for char: " ++ show c

munge :: (MungerM m) => String -> m String
munge sym = concat <$> mapM symWord sym

type MungerM m = TransM () m

munger :: Transformer ()
munger = noopTransformer { tnsTransformName = Just $ \n -> munge n >>= returnT }

runMunger :: (MonadIO m) => IRPackage -> m (Either String IRPackage)
runMunger pkg = runTransformer munger pkg ()
