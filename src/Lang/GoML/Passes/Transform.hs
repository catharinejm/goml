module Lang.GoML.Passes.Transform where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Interregnum hiding (withNewScope)
import           Lang.GoML.Util

type TransM s m = (MonadIO m, MonadReader (TEnv s) m, MonadState s m, MonadError String m)

type TxfDef s m = (TransM s m) => IRGlobalDef -> m (Maybe IRGlobalDef)

data TResult a = TDefault
               | TValue a

data Transformer s =
  Transformer { tnsTransformDefs     ::
                   forall m. (TransM s m) => Maybe ([IRGlobalDef] -> m [IRGlobalDef])
              , tnsTransformTypeDefs ::
                   forall m. (TransM s m) => Maybe ([IRTypeDef] -> m [IRTypeDef])
              , tnsTransformDef      ::
                   forall m. (TransM s m) => Maybe (IRGlobalDef -> m (TResult (Maybe IRGlobalDef)))
              , tnsTransformTypeDef  ::
                   forall m. (TransM s m) => Maybe (IRTypeDef   -> m (TResult (Maybe IRTypeDef)))
              , tnsTransformScope    ::
                   forall m. (TransM s m) => Maybe (IRScope     -> m (TResult IRScope))
              , tnsTransformScopeIdx ::
                   forall m. (TransM s m) => Maybe (IRScopeIdx  -> m (TResult IRScopeIdx))
              , tnsTransformExpr     ::
                   forall m. (TransM s m) => Maybe (IRExpr      -> m (TResult IRExpr))
              , tnsTransformStatement ::
                   forall m. (TransM s m) => Maybe (IRStatement -> m (TResult IRStatement))
              , tnsTransformSchema   ::
                   forall m. (TransM s m) => Maybe (TypeSchema  -> m (TResult TypeSchema))
              , tnsTransformType     ::
                   forall m. (TransM s m) => Maybe (BaseType    -> m (TResult BaseType))
              , tnsTransformFunParam ::
                   forall m. (TransM s m) => Maybe (IRFunParam  -> m (TResult IRFunParam))
              , tnsTransformLetBind ::
                   forall m. (TransM s m) => Maybe (IRLetBind   -> m (TResult IRLetBind))
              , tnsTransformBinding  ::
                   forall m. (TransM s m) => Maybe (IRBinding   -> m (TResult IRBinding))
              , tnsTransformName     ::
                   forall m. (TransM s m) => Maybe (String      -> m (TResult String))
              }

data TEnv s = TEnv { teCurrentScope :: IRScopeIdx
                   , teScopes :: [IRScope]
                   , tePackage :: IRPackage
                   , teTransformer :: Transformer s
                   }

noopTransformer :: Transformer s
noopTransformer = Transformer { tnsTransformDefs = Nothing
                              , tnsTransformTypeDefs = Nothing
                              , tnsTransformDef = Nothing
                              , tnsTransformTypeDef = Nothing
                              , tnsTransformScope = Nothing
                              , tnsTransformScopeIdx = Nothing
                              , tnsTransformExpr = Nothing
                              , tnsTransformStatement = Nothing
                              , tnsTransformSchema = Nothing
                              , tnsTransformType = Nothing
                              , tnsTransformFunParam = Nothing
                              , tnsTransformLetBind = Nothing
                              , tnsTransformBinding = Nothing
                              , tnsTransformName = Nothing
                              }

defaultT :: (TransM s m) => m (TResult a)
defaultT = return TDefault

returnT :: (TransM s m) => a -> m (TResult a)
returnT = return . TValue

joinT :: (TransM s m) => m a -> m (TResult a)
joinT act = act >>= returnT

runTransformer :: MonadIO m => Transformer s -> IRPackage -> s -> m (Either String IRPackage)
runTransformer transformer pkg initialState = do
  (res, p) <- runStateT (runExceptT (doDefs initialState >>= doTypes)) pkg
  return $ fmap (const p) res
  where
    tenv = TEnv (-1) [] pkg transformer
    doDefs st = do
      oldGlobals <- gets (M.elems . irGlobals)
      (newGlobals, st') <- runReaderT (runStateT (transformDefs oldGlobals) st) tenv
      modify $ \s -> s { irGlobals = M.fromList $ map (nameOf &&& id) newGlobals }
      return st'
    doTypes st = do
      oldTypeDefs <- gets (M.elems . irTypeDefs)
      newTypeDefs <- runReaderT (evalStateT (transformTypeDefs oldTypeDefs) st) tenv
      modify $ \s -> s { irTypeDefs = M.fromList $ map (nameOf &&& id) newTypeDefs }

type Trans m s a = ExceptT String (StateT s (ReaderT (TEnv s) m)) a
runTransformStep :: (MonadIO m) => TEnv s -> s -> Trans m s a -> m (Either String (a, s))
runTransformStep env state action = do
  (res, st) <- runReaderT (runStateT (runExceptT action) state) env
  case res of
   Left err -> return $ Left err
   Right res -> return $ Right (res, st)

transformOrDefault :: (TransM s m) => (Transformer s -> Maybe (a -> m (TResult b))) -> (a -> m b) -> a -> m b
transformOrDefault getFn ~deflt input = do
  mfn <- asks (getFn . teTransformer)
  case mfn of
   Nothing -> deflt input
   Just fn -> do
     res <- fn input
     case res of
      TDefault -> deflt input
      TValue v -> return v

withNewScope :: (TransM s m) => IRScope -> m a -> m a
withNewScope scope action = do
  -- sid <- (+1) <$> asks teCurrentScope
  -- let newScope = IRScope sid $ M.fromList $ map (nameOf &&& id) bindings
  scopes <- asks (map irScopeIndex . teScopes)
  liftIO $ putStrLn $+ "adding scope:" +|+ scope +|+ "to" +|+ S scopes
  local (\e -> e { teCurrentScope = (irScopeIndex scope), teScopes = scope : teScopes e }) action

transformDefs :: (TransM s m) => [IRGlobalDef] -> m [IRGlobalDef]
transformDefs input = do
  mfn <- asks (tnsTransformDefs . teTransformer)
  fromMaybe (fmap catMaybes . mapM transformDef) mfn $ input

transformTypeDefs :: (TransM s m) => [IRTypeDef] -> m [IRTypeDef]
transformTypeDefs input = do
  mfn <- asks (tnsTransformTypeDefs . teTransformer)
  fromMaybe (fmap catMaybes . mapM transformTypeDef) mfn $ input

transformDef :: (TransM s m) => IRGlobalDef -> m (Maybe IRGlobalDef)
transformDef = transformOrDefault tnsTransformDef defaultTransformDef

defaultTransformDef :: (TransM s m) => IRGlobalDef -> m (Maybe IRGlobalDef)
defaultTransformDef (IRGlobalFun eid scope name params schema body) = do
  updatedParams <- mapM transformFunParam params
  newScope <- transformScope scope
  Just <$> (IRGlobalFun eid newScope <$> transformName name
                                     <*> pure updatedParams
                                     <*> transformSchema schema
                                     <*> withNewScope newScope (transformStatement body))
defaultTransformDef (IRGlobalVar eid name typ value) =
  Just <$> (IRGlobalVar eid <$> transformName name
                            <*> transformType typ
                            <*> transformExpr value)
defaultTransformDef (IRBuiltin eid name schema) =
  Just <$> (IRBuiltin eid <$> transformName name <*> transformSchema schema)
defaultTransformDef (IRCtor eid name schema) =
  Just <$> (IRCtor eid <$> transformName name <*> transformSchema schema)

transformTypeDef :: (TransM s m) => IRTypeDef -> m (Maybe IRTypeDef)
transformTypeDef = transformOrDefault tnsTransformTypeDef defaultTransformTypeDef

defaultTransformTypeDef :: (TransM s m) => IRTypeDef -> m (Maybe IRTypeDef)
defaultTransformTypeDef (IRTypeDefConcrete eid name typ) =
  Just <$> (IRTypeDefConcrete eid <$> transformName name <*> transformType typ)
defaultTransformTypeDef (IRTypeDefGeneric eid name schema) =
  Just <$> (IRTypeDefGeneric eid <$> transformName name <*> transformSchema schema)

transformScope :: (TransM s m) => IRScope -> m IRScope
transformScope = transformOrDefault tnsTransformScope defaultTransformScope

defaultTransformScope :: (TransM s m) => IRScope -> m IRScope
defaultTransformScope (IRScope idx vars) = do
  let (keys, binds) = unzip $ M.toList vars
  newKeys <- mapM transformName keys
  newBinds <- mapM transformBinding binds
  let newVars = M.fromList $ newKeys `zip` newBinds
  IRScope <$> transformScopeIdx idx <*> pure newVars

transformScopeIdx :: (TransM s m) => IRScopeIdx -> m IRScopeIdx
transformScopeIdx = transformOrDefault tnsTransformScopeIdx defaultTransformScopeIdx

defaultTransformScopeIdx ::(TransM s m) => IRScopeIdx -> m IRScopeIdx
defaultTransformScopeIdx = return

transformExpr :: (TransM s m) => IRExpr -> m IRExpr
transformExpr = transformOrDefault tnsTransformExpr defaultTransformExpr

defaultTransformExpr :: (TransM s m) => IRExpr -> m IRExpr
defaultTransformExpr (IRApp eid fn params typ) =
  IRApp eid <$> transformExpr fn <*> mapM transformExpr params <*> transformType typ
defaultTransformExpr (IRLocalName eid idx name typ) =
  IRLocalName eid idx <$> transformName name <*> transformType typ
defaultTransformExpr (IRGlobalName eid name types schema) =
  IRGlobalName eid <$> transformName name
                   <*> mapM transformType types
                   <*> transformSchema schema
defaultTransformExpr (IRLam eid scope params retType body lamType) = do
  updatedParams <- mapM transformFunParam params
  newScope <- transformScope scope
  IRLam eid newScope updatedParams <$> transformType retType
                                   <*> withNewScope newScope (transformStatement body)
                                   <*> transformType lamType
defaultTransformExpr (IRLet eid scope bindings body letType) = do
  updatedBinds <- mapM transformLetBind bindings
  newScope <- transformScope scope
  IRLet eid newScope updatedBinds <$> withNewScope newScope (transformStatement body)
                                  <*> transformType letType
defaultTransformExpr (IRProp eid recv field typ) =
  IRProp eid <$> transformExpr recv <*> transformName field <*> transformType typ
defaultTransformExpr (IRLit eid lit typ) =
  IRLit eid lit <$> transformType typ

transformStatement :: (TransM s m) => IRStatement -> m IRStatement
transformStatement = transformOrDefault tnsTransformStatement defaultTransformStatement

getScope :: (TransM s m) => IRScopeIdx -> m IRScope
getScope idx = do
  TEnv{..} <- ask
  case drop (fromIntegral (teCurrentScope - idx)) teScopes of
   [] -> throwError $+ "invalid scope index:" +|+ idx +|+
         "(scopes:" +|+ S (map irScopeIndex teScopes) +:+ ")"
   (s:_) -> return s

-- setVar :: (TransM s m) => IRScopeIdx -> String -> IRExpr -> m ()
-- setVar idx name value = do
--   TEnv{..} <- ask
--   newScopes <- case splitAt (fromIntegral (irCurrentScopr - idx)) irScopes of
--    (_, []) -> throwError $+ "invalid scope index:" +|+ idx +|+
--               "(scopes:" +|+ S (map irScopeIndex irScopes) +:+ ")"
--    (before, (s:after)) -> do
--      let bind = IRLetBind name (underlyingType value) value
--          updatedScope = s { irScopeVars = M.insert name bind (irScopeVars s) }
--      return (before ++ updatedScope : after)

defaultTransformStatement :: (TransM s m) => IRStatement -> m IRStatement
-- defaultTransformStatement (IRSetVar idx name value next) = do
--   setVar idx name value
--   IRSetVar idx <$> transformName name <*> transformExpr value <*> transformStatement next
defaultTransformStatement (IRReturn expr) =
  IRReturn <$> transformExpr expr
defaultTransformStatement (IRRecur idx exprs typ) =
  IRRecur idx <$> mapM transformExpr exprs <*> transformType typ


transformSchema :: (TransM s m) => TypeSchema -> m TypeSchema
transformSchema = transformOrDefault tnsTransformSchema defaultTransformSchema

defaultTransformSchema :: (TransM s m) => TypeSchema -> m TypeSchema
defaultTransformSchema (TypeSchema tvs typ) =
  TypeSchema <$> mapM transformName tvs <*> transformType typ

transformType :: (TransM s m) => BaseType -> m BaseType
transformType = transformOrDefault tnsTransformType defaultTransformType

defaultTransformType :: (TransM s m) => BaseType -> m BaseType
defaultTransformType (TypeVar _) = throwError $+ "encountered type var in transform"
defaultTransformType (TypeParam n) = TypeParam <$> transformName n
defaultTransformType (NamedType name types) =
  NamedType <$> transformName name <*> mapM transformType types
defaultTransformType (FunType (Sig types typ)) =
  FunType <$> (Sig <$> mapM transformType types <*> transformType typ)
defaultTransformType (Prim p) = return $ Prim p
defaultTransformType UnitType = return UnitType
defaultTransformType (StructType sname fields types) =
  StructType <$> mapM transformName sname
             <*> mapM transformName fields
             <*> mapM transformType types
defaultTransformType (SliceType typ) = SliceType <$> transformType typ

transformFunParam :: (TransM s m) => IRFunParam -> m IRFunParam
transformFunParam = transformOrDefault tnsTransformFunParam defaultTransformFunParam

defaultTransformFunParam :: (TransM s m) => IRFunParam -> m IRFunParam
defaultTransformFunParam (IRFunParam idx name typ) =
  IRFunParam idx <$> transformName name <*> transformType typ

transformLetBind :: (TransM s m) => IRLetBind -> m IRLetBind
transformLetBind = transformOrDefault tnsTransformLetBind defaultTransformLetBind

defaultTransformLetBind :: (TransM s m) => IRLetBind -> m IRLetBind
defaultTransformLetBind (IRLetBind name typ value) =
  IRLetBind <$> transformName name <*> transformType typ <*> transformExpr value

transformBinding :: (TransM s m) => IRBinding -> m IRBinding
transformBinding = transformOrDefault tnsTransformBinding defaultTransformBinding

defaultTransformBinding :: (TransM s m) => IRBinding -> m IRBinding
defaultTransformBinding (IRFunParam' funParam) = IRFunParam' <$> transformFunParam funParam
defaultTransformBinding (IRLetBind' bind) = IRLetBind' <$> transformLetBind bind

transformName :: (TransM s m) => String -> m String
transformName = transformOrDefault tnsTransformName defaultTransformName

defaultTransformName :: (TransM s m) => String -> m String
defaultTransformName = return
