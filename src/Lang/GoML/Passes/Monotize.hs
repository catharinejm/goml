module Lang.GoML.Passes.Monotize where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Interregnum
import           Lang.GoML.Passes.Transform
import           Lang.GoML.Util

-- ordBaseTypeIdx :: OrdBaseType -> Int
-- ordBaseTypeIdx (O (TypeVar _)) = 0
-- ordBaseTypeIdx (O (TypeParam _)) = 1
-- ordBaseTypeIdx (O (NamedType _ _)) = 2
-- ordBaseTypeIdx (O (FunType _)) = 3
-- ordBaseTypeIdx (O (Prim _)) = 4
-- ordBaseTypeIdx (O UnitType) = 5
-- ordBaseTypeIdx (O (StructType _ _ _)) = 6
-- ordBaseTypeIdx (O (SliceType _)) = 7

-- newtype OrdBaseType = O { unO :: BaseType }
--                     deriving (Show, Eq)
-- instance Ord OrdBaseType where
--   l <= r = if ordBaseTypeIdx l == ordBaseTypeIdx r
--            then case (unO l, unO r) of
--                  (TypeVar i, TypeVar j) -> i <= j
--                  (TypeParam p, TypeParam q) -> p <= q
--                  (NamedType n nts, NamedType m mts) -> n <= m && map O nts <= map O mts
--                  (FunType (Sig lps lr), FunType (Sig rps rr)) -> map O lps <= map O rps && O lr <= O rr
--                  (Prim l, Prim r) -> show l <= show r
--                  (UnitType, UnitType) -> True
--                  (StructType ln lfs lts, StructType rn rfs rts) ->
--                    ln <= rn && lfs <= rfs && map O lts <= map O rts
--                  (SliceType l, SliceType r) -> O l <= O r
--                  _ -> error "impossible!"
--            else ordBaseTypeIdx l < ordBaseTypeIdx r

-- data MState = MState { msSpecials     :: M.Map String (BaseType, Maybe IRGlobalDef)
--                      , msSpecialNames :: M.Map (String, [OrdBaseType]) String
--                      }
-- emptyMState :: MState
-- emptyMState = MState M.empty M.empty

-- type MonoM m = (TransM MState m)

-- monotizer :: IRPackage -> Transformer MState
-- monotizer pkg = (noopTransformer pkg) { tnsTransformDefs = Just monotizeDefs
--                                       , tnsTransformDef = Just monotizeDef
--                                       , tnsTransformExpr = Just monotizeExpr
--                                       }

-- runMonotize :: (MonadIO m) => IRPackage -> m (Either String IRPackage)
-- runMonotize pkg = runTransformer monotizer pkg emptyMState

-- monotizeDefs :: (MonoM m) => [IRGlobalDef] -> m [IRGlobalDef]
-- monotizeDefs defs = do
--   mappedDefs <- catMaybes <$> mapM transformDef defs
--   specials <- mapMaybe snd . M.elems <$> gets msSpecials
--   return $ mappedDefs ++ specials

-- monotizeDef :: (MonoM m) => IRGlobalDef -> m (TResult (Maybe IRGlobalDef))
-- monotizeDef (IRGlobalFun _ _ _ (TypeSchema (_:_) _) _) = returnT Nothing
-- monotizeDef _ = defaultT

-- monotizeExpr :: (MonoM m) => IRExpr -> m (TResult IRExpr)
-- monotizeExpr glob@(IRGlobalName _ _ [] _) = do
--   returnT glob
-- monotizeExpr (IRGlobalName eid name types schema) = do
--   joinT $ specialize eid name types schema
-- monotizeExpr _ = defaultT

-- specializeName :: (MonoM m) => String -> [BaseType] -> m String
-- specializeName name types = do
--   let key = (name, map O types)
--   mspcl <- gets ((M.!? key) . msSpecialNames)
--   case mspcl of
--    Just n -> return n
--    Nothing -> do
--      spcl <- ((name ++ "_" ++ "spclz") ++) . show <$> nextId
--      modify $ \s -> s { msSpecialNames = M.insert key spcl (msSpecialNames s) }
--      return spcl

-- lookupGenericDef :: (MonoM m) => String -> m IRGlobalDef
-- lookupGenericDef name = do
--   mdef <- asks ((M.!? name) . irGlobals . tnsPackage)
--   case mdef of
--    Nothing -> throwError $+ "no such function" +|+ name
--    Just def -> return def

-- specialize :: (MonoM m) => EID -> String -> [BaseType] -> TypeSchema -> m IRExpr
-- specialize eid name types (TypeSchema tvs typ) = do
--   spclName <- specializeName name types
--   mspcl <- gets ((M.!? spclName) . msSpecials)
--   spclType <- case mspcl of
--                Just (spclType, _) -> return spclType
--                Nothing -> do
--                  generic <- lookupGenericDef name
--                  let varMap = M.fromList $ tvs `zip` types
--                  spclType <- substType varMap (underlyingType generic)
--                  modify $ \s ->
--                    s { msSpecials = M.insert spclName (spclType, Nothing) (msSpecials s) }
--                  let specializeGenDef (IRGlobalFun eid _ params (TypeSchema _ typ) body) = do
--                        spcl <- join $ transformExpr <$> substExpr varMap body
--                        sTyp <- substType varMap typ
--                        spParams <- mapM (substParam varMap) params
--                        return $ IRGlobalFun eid spclName spParams (emptySchema sTyp) spcl
--                      specializeGenDef (IRBuiltin eid _ (TypeSchema _ typ)) = do
--                        IRBuiltin eid spclName . emptySchema <$> substType varMap typ
--                      specializeGenDef (IRCtor eid _ (TypeSchema _ typ)) = do
--                        IRCtor eid spclName . emptySchema <$> substType varMap typ
--                  spclDef <- specializeGenDef generic
--                  modify $ \s ->
--                    s { msSpecials = M.adjust (\(t, _) -> (t, Just spclDef)) spclName (msSpecials s) }
--                  return spclType
--   return $ IRGlobalName eid spclName [] (emptySchema spclType)

-- substType :: (MonoM m) => M.Map String BaseType -> BaseType -> m BaseType
-- substType _ (TypeVar _) = throwError "found TypeVar in Montotize"
-- substType typeMap (TypeParam p) =
--   case typeMap M.!? p of
--    Nothing -> throwError $+ "found free type param in Monotize:" +|+ p
--    Just typ -> return typ
-- substType typeMap (NamedType name types) =
--   NamedType name <$> mapM (substType typeMap) types
-- substType typeMap (FunType (Sig argTypes retType)) =
--   FunType <$> (Sig <$> mapM (substType typeMap) argTypes <*> substType typeMap retType)
-- substType _ prim@(Prim _) = return prim
-- substType _ UnitType = return UnitType
-- substType typeMap (StructType sname fields types) =
--   StructType sname fields <$> mapM (substType typeMap) types
-- substType typeMap (SliceType typ) =
--   SliceType <$> substType typeMap typ

-- substExpr :: (MonoM m) => M.Map String BaseType -> IRExpr -> m IRExpr
-- substExpr typeMap (IRApp eid fn args retType) = do
--   let sT = substType typeMap
--       sE = substExpr typeMap
--   IRApp eid <$> sE fn <*> mapM sE args <*> sT retType
-- substExpr typeMap (IRLocalName eid binding) = IRLocalName eid <$> substBinding typeMap binding
-- substExpr typeMap (IRGlobalName eid name typeArgs schema) =
--   specialize eid name typeArgs schema
-- substExpr typeMap (IRLam eid params retType body lamType) = do
--   let sT = substType typeMap
--       sE = substExpr typeMap
--       sP = substParam typeMap
--   IRLam eid <$> mapM sP params <*> sT retType <*> sE body <*> sT lamType
-- substExpr typeMap (IRLet eid bind body typ) = do
--   IRLet eid <$> substLetBind typeMap bind <*> substExpr typeMap body <*> substType typeMap typ
-- substExpr typeMap (IRProp eid recv name typ) = do
--   IRProp eid <$> substExpr typeMap recv <*> pure name <*> substType typeMap typ
-- substExpr typeMap (IRLit eid lit typ) = IRLit eid lit <$> substType typeMap typ

-- substParam :: (MonoM m) => M.Map String BaseType -> IRFunParam -> m IRFunParam
-- substParam typeMap (IRFunParam i name typ) = IRFunParam i name <$> substType typeMap typ

-- substLetBind :: (MonoM m) => M.Map String BaseType -> IRLetBind -> m IRLetBind
-- substLetBind typeMap (IRLetBind name typ value) =
--   IRLetBind name <$> substType typeMap typ <*> substExpr typeMap value

-- substBinding :: (MonoM m) => M.Map String BaseType -> IRBinding -> m IRBinding
-- substBinding typeMap (IRFunParam' param) = IRFunParam' <$> substParam typeMap param
-- substBinding typeMap (IRLetBind' bind) = IRLetBind' <$> substLetBind typeMap bind
