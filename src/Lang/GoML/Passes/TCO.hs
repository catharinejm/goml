module Lang.GoML.Passes.TCO where

import Control.Monad.IO.Class
import Lang.GoML.AST
import Lang.GoML.Interregnum
import Lang.GoML.Passes.Transform

data TCOState = TCOState { tcosSeen :: [String]
                         }
              deriving (Show)
emptyTCOState :: TCOState
emptyTCOState = TCOState []

type TCOM m = TransM TCOState m

tCO :: Transformer TCOState
tCO = noopTransformer -- { tnsPackage = pkg
                      -- }

runTCO :: (MonadIO m) => IRPackage -> m (Either String IRPackage)
runTCO pkg = runTransformer tCO pkg emptyTCOState

