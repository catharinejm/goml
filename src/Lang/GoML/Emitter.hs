module Lang.GoML.Emitter where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Env
import           Lang.GoML.Util

type EmitterM m = (MonadIO m, MonadReader Env m, MonadError String m)
-- type DeclEmitterM m = (EmitterM m, MonadState EState m)

-- data EState = EState { esDeclaredNames :: [String]
--                      }

-- runEmitter :: (MonadIO m) => Env -> m (Either String String)
-- runEmitter env = runReaderT (runExceptT buildGoSource) env

-- buildGoSource :: (EmitterM m) => m String
-- buildGoSource = do
--   pnameS <- ("package " ++) . tos <$> asks packageName
--   imports <- join $ mapM emitImport <$> (M.elems <$> asks envImports)
--   types <- join $ mapM emitTypeDef <$> (M.elems <$> asks typeDefs)
--   decls <- join $ mapM emitDecl <$> (M.elems <$> asks functionDecls)
--   return (unlines (pnameS:imports) ++ unlines types ++ "\n" ++ unlines decls)

-- emitImport :: (EmitterM m) => Import -> m String
-- emitImport (Import alias path) = do
--   return $ "import " ++ alias ++ " \"" ++ path ++ "\""

-- emitTypeDef :: (EmitterM m) => TypeDef -> m String
-- emitTypeDef td@(TypeDef eid name schema) = do
--   liftIO $ putStrLn $+ "emitting:" +|+ S td
--   schemaStr <- emitSchema schema
--   return $ "type " ++ name ++ " " ++ schemaStr

-- emitSchema :: (EmitterM m) => TypeSchema -> m String
-- emitSchema (TypeSchema _ typ) = emitType typ

-- lookupType :: (EmitterM m) => Expr -> m BaseType
-- lookupType expr = (M.! eID expr) <$> asks envExprTypes

-- lookupFunDecl :: (EmitterM m) => String -> m FunDecl
-- lookupFunDecl name = (M.! name) <$> asks functionDecls

-- lookupTypeDefSchema :: (EmitterM m) => String -> m TypeSchema
-- lookupTypeDefSchema name = typeSchema . (M.! name) <$> asks typeDefs

-- spaceZip :: [String] -> [String] -> [String]
-- spaceZip lhs rhs = map (\(l, r) -> l ++ " " ++ r) (lhs `zip` rhs)

-- emitArgList :: (EmitterM m) => [String] -> [BaseType] -> m String
-- emitArgList names types = intercalate ", " . (names `spaceZip`) <$> mapM emitType types

-- genArgNames :: [String]
-- genArgNames = [l++n | n <- "":map show [(1::Int)..], l <- map (:[]) ['a'..'z']]

-- emitType :: (EmitterM m) => BaseType -> m String
-- emitType (TypeVar _) = throwError "unbound type variable encountered in code emission"
-- emitType (SliceType typ) = ("[]"++) <$> emitType typ
-- -- emitType _ = return "interface{}"
-- emitType (TypeParam p) = throwError "cannot emit type parameter"
-- emitType (NamedType n _) = return n
-- emitType (FunType (Sig args ret)) = do
--   retStr <- emitType ret
--   argList <- emitArgList genArgNames args
--   return $ "func(" ++ argList ++ ") " ++ retStr
-- emitType (Prim p) = return $ tos p
-- emitType UnitType = return "struct{}"
-- emitType (StructType mname fields typs) = do
--   typStrs <- mapM emitType typs
--   let fieldStrs :: [String]
--       fieldStrs = fields `spaceZip` typStrs
--       lines :: [String]
--       lines = "struct {" : (map ('\t':) fieldStrs) ++ ["}"]
--   return $ unlines lines

-- emitDecl :: (EmitterM m) => FunDecl -> m String
-- emitDecl (FunDecl _ name (TypeSchema _ UnitType) [] body) = do
--   bodyS <- emitExpr body
--   return $ unlines [ "func " ++ name ++ "() {"
--                    , "\t" ++ bodyS
--                    , "}"
--                    ]
-- emitDecl (FunDecl _ name schema [] body) = do
--   t <- emitSchema schema
--   b <- emitExpr body
--   return $ "var " ++ name ++ " " ++ t ++ " = " ++ b
-- emitDecl (FunDecl _ name (TypeSchema tvs ftyp) args body) = do
--   let FunType (Sig argTypes retType) = ftyp
--   argList <- emitArgList args argTypes
--   retStr <- emitType retType
--   bodyS <- emitExpr body
--   return $ unlines [ "func " ++ name ++ "(" ++ argList ++ ") " ++ retStr ++ " {"
--                    , "\t" ++ (if isUnit retType then "" else "return ") ++ bodyS
--                    , "}"
--                    ]
-- emitDecl (BuiltinDecl _ _ _) = return ""
-- emitDecl (CtorDecl _ _ _) = return ""

-- emitExpr :: (EmitterM m) => Expr -> m String
-- emitExpr app@(App _ fn _ args) = do
--   fnType <- case fn of
--              Name _ (Global n) -> underlyingType <$> lookupFunDecl n
--              other -> lookupType other
--   declaredRet <- case fnType of
--                   FunType (Sig _ ret) -> return ret
--                   typ -> return typ
--   retTyp <- lookupType app
--   fnS <- emitExpr fn
--   argsS <- intercalate ", " <$> mapM emitExpr args
--   liftIO $ putStrLn $+ "declaredRet:" +|+ S declaredRet
--   castS <- if isParam declaredRet && not (isParam retTyp)
--            then do rs <- emitType retTyp
--                    return $ ".(" ++ rs ++ ")"
--            else return ""
--   return $ fnS ++ "(" ++ argsS ++ ")" ++ castS
-- emitExpr (Lam _ (Sig argTypes retType) args body) = do
--   argList <- emitArgList args argTypes
--   retS <- emitType retType
--   bodyS <- emitExpr body
--   return $ unlines [ "func(" ++ argList ++ ") " ++ retS ++ " {"
--                    , "\t" ++ (if isUnit retType then "" else "return ") ++ bodyS
--                    , "}"
--                    ]
-- emitExpr (Let _ name value typ body) = do
--   valueS <- emitExpr value
--   typS <- emitType typ
--   bodyTyp <- lookupType body
--   bodyTypS <- emitType bodyTyp
--   bodyS <- emitExpr body
--   return $ unlines [ "func(" ++ name ++ " " ++ typS ++ ") " ++ bodyTypS ++ " {"
--                    , "\treturn " ++ bodyS
--                    , "}(" ++ valueS ++ ")"
--                    ]
-- emitExpr (Prop _ recv fname) = do
--   StructType mStructName fields ftyps <- lookupType recv
--   let Just fieldIdx = elemIndex fname fields
--   ftypeS <- emitType (ftyps !! fieldIdx)
--   needsCast <- case mStructName of
--     Just structName -> do
--       StructType _ _ typs <- underlyingType <$> lookupTypeDefSchema structName
--       return $ isParam (typs !! fieldIdx) && not (isParam (ftyps !! fieldIdx))
--     Nothing -> return False
--   recvS <- emitExpr recv
--   return $ recvS ++ "." ++ fname ++ if needsCast then ".("++ftypeS++")" else ""
-- emitExpr (Name _ n) = return $ nameOf n
-- emitExpr (Lit _ UnitLit) = return "struct{}{}"
-- emitExpr (Lit _ lit) = return $ tos lit
-- emitExpr pre@(Pre _ _) = throwError $+ "encountered unexpected pre-expression in code emission:" +|+ S pre
