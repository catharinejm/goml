module Lang.GoML.Interregnum where

import           Control.Arrow
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Strict as M
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Env
import           Lang.GoML.Util

data IRLetBind = IRLetBind String BaseType IRExpr
               deriving (Show, Eq)

instance Named IRLetBind where
  nameOf (IRLetBind name _ _) = name

instance HasType IRLetBind where
  underlyingType (IRLetBind _ typ _) = typ

data IRFunParam = IRFunParam Int String BaseType
               deriving (Show, Eq)

instance Named IRFunParam where
  nameOf (IRFunParam _ name _) = name

instance HasType IRFunParam where
  underlyingType (IRFunParam _ _ typ) = typ

data IRBinding = IRFunParam' IRFunParam
               | IRLetBind' IRLetBind
               deriving (Show, Eq)

instance Named IRBinding where
  nameOf (IRFunParam' param) = nameOf param
  nameOf (IRLetBind' bind) = nameOf bind

instance HasType IRBinding where
  underlyingType (IRFunParam' param) = underlyingType param
  underlyingType (IRLetBind' bind) = underlyingType bind

data IRGlobalDef = IRGlobalFun EID IRScope String [IRFunParam] TypeSchema IRStatement
                 | IRGlobalVar EID String BaseType IRExpr
                 | IRBuiltin EID String TypeSchema
                 | IRCtor EID String TypeSchema
                 deriving (Show, Eq)

instance Stringable IRGlobalDef where
  tos (IRGlobalFun _ _ name params (TypeSchema tvs typ) body) =
    "defn " ++ name ++ tvstr ++ "(" ++ intercalate "," (map pstr params) ++ ") -> " ++ tos retType ++ " {\n  " ++ tos body ++ "\n}"
    where
      pstr (IRFunParam _ name typ) = name ++ " : " ++ tos typ
      tvstr = case tvs of
               [] -> ""
               tvs -> "[" ++ intercalate "," tvs ++ "]"
      FunType (Sig _ retType) = typ
  tos (IRGlobalVar _ name typ value) =
    "var " ++ name ++ " : " ++ tos typ ++ " = " ++ tos value ++ ";"
  tos (IRBuiltin _ name schema) =
    "builtin " ++ name ++ " : " ++ tos schema
  tos (IRCtor _ name schema) =
    "ctor " ++ name ++ " : " ++ tos schema

instance HasEID IRGlobalDef where
  eID (IRGlobalFun eid _ _ _ _ _) = eid
  eID (IRGlobalVar eid _ _ _) = eid
  eID (IRBuiltin eid _ _) = eid
  eID (IRCtor eid _ _) = eid

instance Named IRGlobalDef where
  nameOf (IRGlobalFun _ _ name _ _ _) = name
  nameOf (IRGlobalVar _ name _ _) = name
  nameOf (IRBuiltin _ name _) = name
  nameOf (IRCtor _ name _) = name

instance HasSchema IRGlobalDef where
  typeSchema (IRGlobalFun _ _ _ _ schema _) = schema
  typeSchema (IRGlobalVar _ _ typ _) = emptySchema typ
  typeSchema (IRBuiltin _ _ schema) = schema
  typeSchema (IRCtor _ _ schema) = schema

data IRImportedName = IRImportedName EID String BaseType
                    deriving (Show, Eq)

instance HasEID IRImportedName where
  eID (IRImportedName eid _ _) = eid

instance Named IRImportedName where
  nameOf (IRImportedName _ name _) = name

newtype IRScopeIdx = IRScopeIdx Int
                   deriving (Show, Eq, Ord, Num, Real, Enum, Integral)

instance Stringable IRScopeIdx where
  tos (IRScopeIdx i) = tos i

type IRScopeVars = M.Map String IRBinding

data IRScope = IRScope { irScopeIndex :: IRScopeIdx
                       -- , irScopeName  :: String
                       , irScopeVars  :: IRScopeVars
                       }
             deriving (Show, Eq)

instance Stringable IRScope where
  tos (IRScope idx vars) =
    "scope " ++ tos idx ++ "{" ++ bindString ++ "\n}"
    where
      bindString = intercalate "\n  " ("":map bstr (M.elems vars))
      bstr (IRFunParam' (IRFunParam idx name typ)) =
        name ++ " : " ++ tos typ ++ " = param[" ++ show idx ++ "]"
      bstr (IRLetBind' (IRLetBind name str value)) =
        name ++ " : " ++ tos (underlyingType value) ++ " = " ++ tos value

data IRStatement = IRReturn IRExpr
                 | IRRecur IRScopeIdx [IRExpr] BaseType
                 -- | IRSetVar IRScopeIdx String IRExpr IRStatement
                 deriving (Show, Eq)

instance HasType IRStatement where
  underlyingType (IRReturn expr) = underlyingType expr
  underlyingType (IRRecur _ _ typ) = typ

instance Stringable IRStatement where
  tos (IRReturn expr) = "return " ++ tos expr ++ ";"
  tos (IRRecur (IRScopeIdx idx) exprs _) =
    "recur[" ++ show idx ++ "](" ++ intercalate "," (map tos exprs) ++ ");"

data IRExpr = IRApp EID IRExpr [IRExpr] BaseType
            | IRLocalName EID IRScopeIdx String BaseType
            | IRGlobalName EID String [BaseType] TypeSchema
            | IRLam EID IRScope [IRFunParam] BaseType IRStatement BaseType
            | IRLet EID IRScope [IRLetBind] IRStatement BaseType
            | IRProp EID IRExpr String BaseType
            | IRLit EID Literal BaseType
            deriving (Show, Eq)

instance Stringable IRExpr where
  tos (IRApp _ fn args _) =  tos fn ++ "(" ++ intercalate "," (map tos args) ++ ")"
  tos (IRLocalName _ _ name _) = name
  tos (IRGlobalName _ name types _) = name ++ tvarStr
    where
      tvarStr = case types of
                 [] -> ""
                 types -> "[" ++ intercalate "," (map tos types) ++ "]"
  tos (IRLam _ _ params _ body _) =
    "fn(" ++ intercalate "," (map pstr params) ++ ")" ++ "{\n  " ++ tos body ++ "\n}"
    where
      pstr (IRFunParam _ name typ) = name ++ " : " ++ tos typ
  tos (IRLet _ _ binds body _) =
    "let{ " ++ intercalate "\n     " (map bstr binds) ++ " \n} in " ++ tos body
    where
      bstr (IRLetBind name typ value) = name ++ " : " ++ tos typ ++ " = " ++ tos value
  tos (IRProp _ recv field _) = tos recv ++ "." ++ field
  tos (IRLit _ lit _) = tos lit

instance HasEID IRExpr where
  eID (IRApp eid _ _ _) = eid
  eID (IRLocalName eid _ _ _) = eid
  eID (IRGlobalName eid _ _ _) = eid
  eID (IRLam eid _ _ _ _ _) = eid
  eID (IRLet eid _ _ _ _) = eid
  eID (IRProp eid _ _ _) = eid
  eID (IRLit eid _ _) = eid

instance HasType IRExpr where
  underlyingType (IRApp _ _ _ typ) = typ
  underlyingType (IRLocalName _ _ _ typ) = typ
  underlyingType (IRGlobalName _ _ _ schema) = underlyingType schema
  underlyingType (IRLam _ _ _ _ _ typ) = typ
  underlyingType (IRLet _ _ _ _ typ) = typ
  underlyingType (IRProp _ _ _ typ) = typ
  underlyingType (IRLit _ _ typ) = typ

data IRTypeDef = IRTypeDefConcrete EID String BaseType
               | IRTypeDefGeneric EID String TypeSchema
               deriving (Show, Eq)

instance HasEID IRTypeDef where
  eID (IRTypeDefConcrete eid _ _) = eid
  eID (IRTypeDefGeneric eid _ _) = eid

instance Named IRTypeDef where
  nameOf (IRTypeDefConcrete _ name _) = name
  nameOf (IRTypeDefGeneric _ name _) = name

data IRPackage = IRPackage { irPackageName :: PackageName
                           , irImports     :: M.Map String Import
                           , irGlobals     :: M.Map String IRGlobalDef
                           , irTypeDefs    :: M.Map String IRTypeDef
                           }
               deriving (Show, Eq)

data IRState = IRState { irCurrentScope :: IRScopeIdx
                       , irScopes :: [IRScope]
                       }
emptyIRState :: IRState
emptyIRState = IRState 0 []

initIRState :: IRScope -> IRState
initIRState = IRState 0 . (:[])

type InterM m = (MonadIO m, MonadReader Env m, MonadError String m)
type InterExprM m = (InterM m, MonadState IRState m)

buildIRPackage :: (MonadIO m) => Env -> m (Either String IRPackage)
buildIRPackage env = runReaderT (runExceptT processEnv) env

processEnv :: (InterM m) => m IRPackage
processEnv = IRPackage <$> asks packageName <*> asks envImports <*> scrapeGlobals <*> scrapeTypeDefs
  where
    scrapeGlobals = do
      (keys, decls) <- unzip . M.toList <$> asks functionDecls
      globs <- mapM processGlobal decls
      return $ M.fromList (keys `zip` globs)
    scrapeTypeDefs = do
      (keys, tds) <- unzip . M.toList <$> asks typeDefs
      irTds <- mapM processTypeDef tds
      return $ M.fromList (keys `zip` irTds)

mkParams :: [(String, BaseType)] -> [IRFunParam]
mkParams namesAndTypes = map mk' (namesAndTypes `zip` [0..])
  where
    mk' ((n, t), i) = IRFunParam i n t

processGlobal :: (InterM m) => FunDecl -> m IRGlobalDef
-- processGlobal (FunDecl eid name (TypeSchema _ UnitType) [] body) = do
--   let schema = emptySchema (FunType (Sig [] UnitType))
--   IRGlobalFun eid name [] schema <$> evalStateT (processExpr body) M.empty
processGlobal (FunDecl eid name (TypeSchema _ typ) [] body) = do
  IRGlobalVar eid name typ <$> evalStateT (processExpr body) emptyIRState
processGlobal decl@(FunDecl eid name schema argNames body) = do
  let FunType (Sig argTypes retTyp) = underlyingType schema
      params = mkParams (argNames `zip` argTypes)
      bodyScope = IRScope 0 $ M.fromList (argNames `zip` map IRFunParam' params)
  bodyExpr <- evalStateT (processExpr body) $ initIRState bodyScope
  return $ IRGlobalFun eid bodyScope name params schema (IRReturn bodyExpr)
processGlobal (BuiltinDecl eid name schema) = do
  return $ IRBuiltin eid name schema
processGlobal (CtorDecl eid name schema) = do
  return $ IRCtor eid name schema

processTypeDef :: (InterM m) => TypeDef -> m IRTypeDef
processTypeDef (TypeDef eid name (TypeSchema [] typ)) = do
  return $ IRTypeDefConcrete eid name typ
processTypeDef (TypeDef eid name schema) = do
  return $ IRTypeDefGeneric eid name schema

lookupFunctionSchema :: (InterM m) => Expr -> m TypeSchema
lookupFunctionSchema (Name _ (Global name _)) = lookupGlobalExprSchema name
lookupFunctionSchema expr = emptySchema <$> lookupExprType expr

lookupExprType :: (InterM m, HasEID e, Show e) => e -> m BaseType
lookupExprType e = do
  typ <- asks ((M.!? eID e) . envExprTypes)
  case typ of
   Just t -> return t
   Nothing -> throwError $+ "cannot find type for expr:" +|+ S e

lookupGlobalExprSchema :: (InterM m) => String -> m TypeSchema
lookupGlobalExprSchema name = do
  typ <- asks ((M.!? name) . functionTypes)
  case typ of
   Just t -> return t
   Nothing -> throwError $+ "no such function:" +|+ name

lookupLocalBinding :: (InterExprM m) => String -> m (IRScopeIdx, IRBinding)
lookupLocalBinding name = do
  scopes <- gets irScopes
  case find (M.member name . irScopeVars) scopes of
   Nothing -> throwError $+ "no such local:" +|+ name
   Just IRScope{..} -> return (irScopeIndex, irScopeVars M.! name)

withLocalState :: (InterExprM m) => (IRState -> IRState) -> m a -> m a
withLocalState f action = do
  st <- get
  modify f
  res <- action
  modify (const st)
  return res

withNewScope :: (InterExprM m) => IRScope -> m a -> m a
withNewScope scope action = do
  oldState <- get
  modify $ \s -> s { irCurrentScope = irScopeIndex scope
                   , irScopes = scope : irScopes s
                   }
  res <- action
  modify (const oldState)
  return res

newScope :: (InterExprM m) => M.Map String IRBinding -> m IRScope
newScope = (IRScope <$> ((+1) <$> gets irCurrentScope) <*>) . pure

processExpr :: (InterExprM m) => Expr -> m IRExpr
processExpr (App eid fn args) = do
  IRApp eid <$> processExpr fn <*> mapM processExpr args <*> lookupExprType eid
processExpr (Lam eid (Sig paramTypes retType) paramNames body) = do
  let params = mkParams $ paramNames `zip` paramTypes
  lamScope <- newScope $ M.fromList $ paramNames `zip` map IRFunParam' params
  bodyExpr <- withNewScope lamScope (processExpr body)
  IRLam eid lamScope params retType (IRReturn bodyExpr) <$> lookupExprType eid
processExpr (Let eid name value typ body) = do
  irValue <- processExpr value
  let bind = IRLetBind name typ irValue
  letScope <- newScope $ M.singleton name (IRLetBind' bind)
  bodyExpr <- withNewScope letScope $ processExpr body
  IRLet eid letScope [IRLetBind name typ irValue] (IRReturn bodyExpr) <$> lookupExprType eid
processExpr (Prop eid recv field) = do
  IRProp eid <$> processExpr recv <*> pure field <*> lookupExprType eid
processExpr (Name eid (Local name)) = do
  (scopeIdx, underlyingType -> typ) <- lookupLocalBinding name
  return $ IRLocalName eid scopeIdx name typ
processExpr (Name eid (Global name types)) = do
  IRGlobalName eid name types <$> lookupGlobalExprSchema name
processExpr (Lit eid lit) = do
  IRLit eid lit <$> lookupExprType eid
processExpr pre@(Pre _ _) = do
  throwError $+ "encountered unexpected pre-expression while generating IR:" +|+ S pre
