module Lang.GoML.Typecheck ( module Lang.GoML.Typecheck
                           , module Lang.GoML.Typecheck.Prims
                           ) where

import           Control.Monad.Except
import           Control.Monad.RWS.Strict
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.List
import qualified Data.Map.Merge.Strict as M
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.String.Util
import           Lang.GoML.AST
import           Lang.GoML.Typecheck.Prims
import           Lang.GoML.Util
import           Lang.GoML.Env

data TState = TState { tsTypeVars    :: M.Map Int BaseType
                     , tsConstraints :: M.Map Int [Constraint]
                     , tsExprTypes   :: M.Map EID BaseType
                     }
            deriving (Show)

emptyState :: TState
emptyState = TState { tsTypeVars = M.empty
                    , tsConstraints = M.empty
                    , tsExprTypes = M.empty
                    }

type TyperM m = (MonadIO m, MonadReader Env m, MonadState TState m, MonadError String m)
type TyperREM m = (MonadReader Env m, TyperErrorM m)
type TyperErrorM m = (MonadError String m)

mkTypeName :: (MonadIO io) => io String
mkTypeName = nextId >>= return . ("t"++) . show

mkTypeVar :: (MonadIO m) => m BaseType
mkTypeVar = nextId >>= return . TypeVar

mkTypeVarAndName :: (MonadIO io) => io (BaseType, String)
mkTypeVarAndName = nextId >>= \n -> return (TypeVar n, "t"++show n)

buildEnv :: (MonadIO m, MonadError String m) => Package -> m Env
buildEnv (Package name imports decls) = do
  (_, env) <- runStateT (mapM_ addDecl decls) (emptyEnv name imports)
  validateNamedTypes env
  return env

validateNamedTypes :: (MonadError String m) => Env -> m ()
validateNamedTypes env = do
  validateTypes (typeDefs env)
  validateDecls (functionDecls env)
  where
    assertExists tname = do
      let inTypeDefs = M.member tname (typeDefs env)
          inPrims = M.member tname prims
      unless (inTypeDefs || inPrims) (throwError $+ "undefined type" +|+ tname +|+ "(validation)")
    validateTypes = mapM_ validateType . map (underlyingType . snd) . M.toList
    validateType (NamedType name typs) = do
      assertExists name
      mapM_ validateType typs
    validateType (FunType (Sig argTypes retType)) = do
      mapM_ validateType argTypes
      validateType retType
    validateType (StructType _ _ typs) = mapM_ validateType typs
    validateType _ = return ()
    validateDecls = mapM_ validateDecl . map snd . M.toList
    validateDecl fd@(FunDecl _ _ _ _ body) = do
      validateType (underlyingType fd)
      validateExpr body
    validateDecl builtin@(BuiltinDecl _ _ _) = validateType (underlyingType builtin)
    validateDecl ctor@(CtorDecl _ _ _) = validateType (underlyingType ctor)
    validateExpr (App _ fn args) = do
      validateExpr fn
      mapM_ validateExpr args
    validateExpr (Lam _ (Sig paramTypes retType) _ body) = do
      mapM_ validateType paramTypes
      validateType retType
      validateExpr body
    validateExpr (Let _ _ bind bindType body) = do
      validateExpr bind
      validateType bindType
      validateExpr body
    validateExpr (Name _ name) = validateName name
    validateExpr (Pre _ (PELet _ bind body)) = do
      validateExpr bind
      validateExpr body
    validateExpr (Pre _ (PELam _ body)) = do
      validateExpr body
    validateExpr (Pre _ (PEOp lhs (NameExpr _ name) rhs)) = do
      validateExpr lhs
      validateName name
      validateExpr rhs
    validateExpr (Pre _ (PEName name)) = validateName name
    validateExpr (Prop _ recv _) = validateExpr recv
    validateExpr (Lit _ _) = return ()
    validateName (Local _) = return ()
    validateName (Global _ types) = mapM_ validateType types

validateDeclArity :: (MonadIO m, MonadError String m) => FunDecl -> m ()
validateDeclArity (FunDecl _ fname (TypeSchema _ (FunType (Sig argTypes _))) args _) =
  when (length argTypes /= length args) $
  throwError $+ "wrong number of function parameters in" +|+ fname +:+ ", expected:" +|+ length argTypes +:+ ", got:" +|+ length args
validateDeclArity (FunDecl _ fname _ (_:_) _) = throwError $+ "definition of" +|+ fname +|+ "should have no parameters"
validateDeclArity _ = return ()

assertNewName :: (MonadIO m, MonadState Env m, MonadError String m) => String -> m ()
assertNewName name = do
  tds <- gets typeDefs
  decls <- gets functionDecls
  ftypes <- gets functionTypes
  when (M.member name tds || M.member name decls || M.member name ftypes) $
    throwError $+ "duplicate definition of" +|+ name

addDecl :: (MonadIO m, MonadState Env m, MonadError String m) => TopLevel -> m ()
addDecl (TLFun decl) = do
  let fname = nameOf decl
      schema = typeSchema decl
  validateDeclArity decl
  modify $ \s -> s { functionTypes = M.insert fname schema (functionTypes s)
                   , functionDecls = M.insert fname decl (functionDecls s)
                   }
addDecl (TLType typeDef) = do
  let name = nameOf typeDef
      schema = typeSchema typeDef
  ctorEID <- genEID
  let ctorSchema = case schema of
                    TypeSchema tvs (StructType _ _ typs) ->
                      let retType = (NamedType name (map TypeParam tvs))
                      in TypeSchema tvs (FunType (Sig typs retType))
                    TypeSchema tvs nonStruct ->
                      let retType = (NamedType name (map TypeParam tvs))
                      in TypeSchema tvs (FunType (Sig [nonStruct] retType))
      ctor = CtorDecl ctorEID name ctorSchema
  modify $ \s -> s { typeDefs = M.insert name typeDef (typeDefs s)
                   , functionTypes = M.insert name ctorSchema (functionTypes s)
                   , functionDecls = M.insert name ctor (functionDecls s)
                   }
addDecl (TLFix fixity) = do
  fixits <- gets fixities
  when (M.member (nameOf fixity) fixits) $
    throwError $+ "duplicate fixity definition for operator" +|+ (nameOf fixity)
  modify $ \s -> s { fixities = M.insert (nameOf fixity) fixity fixits }

preTypeName :: (TyperM m) => Name -> m Name
preTypeName glob@(Global name _) = do
  (TypeSchema tparams _) <- lookupTypeSchema glob
  Global name <$> mapM (\_ -> mkTypeVar) tparams
preTypeName local = return local

preType :: (TyperM m) => Expr -> m Expr
preType (Pre eid (PELet name bind body)) = do
  tv <- mkTypeVar
  withLocal name tv $ Let eid name <$> preType bind <*> pure tv <*> preType body
preType (Pre eid (PELam args body)) = do
  typVars <- mapM (\_ -> mkTypeVar) args
  retTypVar <- mkTypeVar
  let sig = Sig typVars retTypVar
  withLocals (args `zip` typVars) $ Lam eid sig args <$> preType body
preType (Pre eid (PEName name)) = Name eid <$> preTypeName name
preType opExpr@(Pre eid (PEOp lhs (NameExpr nEID op) rhs)) = do
  fixity <- fromMaybe (InfixL 9 (nameOf op)) <$> ((M.!? nameOf op) <$> asks fixities)
  let exprs = extractExprs [] rhs
      ops = extractOps [] rhs
  when (any ((/= nameOf op) . nameOf . fst) ops) $
    throwError $ "Operator precedence is not yet supported. Please group expressions containing different operators"
  let opExpr = Pre nEID (PEName op)
      mkOpExpr (op, eid) = Pre eid (PEName op)
  res <- case fixity of
   (InfixL _ _) -> do
     let baseApp = App eid opExpr [lhs, (head exprs)]
         foldApps :: Expr -> ((Name, EID), Expr) -> Expr
         foldApps left (op', right) = App eid (mkOpExpr op') [left, right]
     return $ foldl' foldApps baseApp (ops `zip` tail exprs)
   (InfixR _ _) -> do
     let baseExpr = last exprs
         foldApps :: (Expr, (Name, EID)) -> Expr -> Expr
         foldApps (left, op') right = App eid (mkOpExpr op') [left, right]
     return $ foldr foldApps baseExpr ((lhs:init exprs) `zip` ((op, nEID):ops))
   (Infix  _ _) -> case ops of
                    [_] -> return $ App eid opExpr [lhs, rhs]
                    _ -> throwError "No more than one non-associative operator may appear in a single expression"
  preType res
  where
    extractExprs es (Pre _ (PEOp lhs _ rhs)) = extractExprs (lhs:es) rhs
    extractExprs es e = reverse (e:es)
    extractOps ops (Pre eid (PEOp _ (NameExpr _ op) rhs)) = extractOps ((op, eid):ops) rhs
    extractOps ops _ = reverse ops
preType (App eid fn args) = App eid <$> preType fn <*> mapM preType args
preType (Lam eid sig args body) = Lam eid sig args <$> preType body
preType (Let eid name value typ body) = Let eid name <$> preType value <*> pure typ <*> preType body
preType (Prop eid recv field) = Prop eid <$> preType recv <*> pure field
preType n@(Name _ _) = return n
preType l@(Lit _ _) = return l

runTyper :: (MonadIO io) => Env -> io (Either String Env)
runTyper env = runTyper' run' env
  where
    run' = do
      (names, decls) <- unzip . M.toList <$> asks functionDecls
      (updatedDecls, typeMaps) <- unzip <$> mapM typecheckDecl decls
      return $ env { envExprTypes = foldr M.union M.empty typeMaps
                   , functionDecls = M.fromList (names `zip` updatedDecls)
                   }

type Typer m a = ExceptT String (ReaderT Env m) a

runTyper' :: (MonadIO io) => Typer io a -> Env -> io (Either String a)
runTyper' typer env = runReaderT (runExceptT typer) env

type ExprTyper m a = ExceptT String (RWST Env () TState m) a

runExprTyper :: (MonadIO io) => ExprTyper io a -> Env -> io (Either String a, TState)
runExprTyper typer env = do
  (res, st, _) <- runRWST (runExceptT typer) env emptyState
  return (res, st)

errMaybe :: (TyperErrorM m, Stringable s) => s -> Maybe a -> m a
errMaybe msg Nothing = throwError $ tos msg
errMaybe _ (Just a) = return a

lookupType :: (TyperM m) => String -> m TypeSchema
lookupType tname = do
  mtyp <- (M.!? tname) <$> asks typeDefs
  case mtyp of
   Nothing -> do
     maybe (throwError $+ "undefined type" +|+ tname +|+ "(typechecking)") (return . typeSchema) (prims M.!? tname)
   Just typ -> return $ typeSchema typ

tryGetParamType :: (TyperM m) => String -> m (Maybe BaseType)
tryGetParamType pname = do
  mtyp <- asks localNames >>= return . (M.!? pname)
  case mtyp of
   Nothing -> return Nothing
   Just typ -> chaseType typ >>= return . Just

getParamType :: (TyperM m) => String -> m BaseType
getParamType pname = tryGetParamType pname >>= errMaybe ("undefined parameter name:" +|+ pname)

tryGetFunctionType :: (TyperM m) => String -> m (Maybe TypeSchema)
tryGetFunctionType fname = asks functionTypes >>= return . (M.!? fname)

getFunctionType :: (TyperM m) => String -> m TypeSchema
getFunctionType fname = tryGetFunctionType fname >>= errMaybe ("undefined function name:" +|+ fname)

lookupTypeSchema :: (TyperM m) => Name -> m TypeSchema
lookupTypeSchema (Local name) = getParamType name >>= return . TypeSchema []
lookupTypeSchema (Global name _) = getFunctionType name

lookupTypeOfBinding :: (TyperM m) => Name -> m BaseType
lookupTypeOfBinding (Local name) = getParamType name
lookupTypeOfBinding (Global name typVars) = do
  schema <- getFunctionType name
  subst schema typVars

litType :: Literal -> BaseType
litType (IntLit _) = Prim IntT
litType (FloatLit _) = Prim Float64T
litType (StringLit _) = Prim StringT
litType (CharLit _) = NamedType "rune" []
litType UnitLit = UnitType

typecheckDecl :: forall m. (MonadIO m, MonadReader Env m, MonadError String m) => FunDecl -> m (FunDecl, M.Map EID BaseType)
typecheckDecl fd@(FunDecl eid _ (TypeSchema _ fnType) args body) = do
  (newLocals, retTyp) <- case fnType of
                          FunType (Sig argTypes retTyp) -> return (args `zip` argTypes, retTyp)
                          typ -> return ([], typ)
  local (\e -> e { localNames = foldr (\(k, v) m -> M.insert k v m) (localNames e) newLocals }) $ do
    (preTypedBody, st) <- runStateT (preType body) emptyState
    (bodyTyp, st) <- runStateT (typeof preTypedBody) st
    (_, st) <- runStateT (unifyTypes retTyp bodyTyp) st
    -- liftIO $ do
    --   putStrLn $+ "Original body:" +|+ S body
    --   putStrLn $+ "After preType:" +|+ S preTypedBody
    --   putStrLn $+ "Collected types:" +|+ S (tsExprTypes st)
    chasedPreTypedBody <- evalStateT (chaseExprTypes preTypedBody) st
    return (fd { funBody = chasedPreTypedBody }, M.union (tsExprTypes st) (M.fromList [(eid, fnType)]))
typecheckDecl bd@(BuiltinDecl eid _ _) = return (bd, M.singleton eid (underlyingType bd))
typecheckDecl cd@(CtorDecl eid _ _) = return (cd, M.singleton eid (underlyingType cd))

lookupConstraints :: (TyperM m) => BaseType -> m [Constraint]
lookupConstraints (TypeVar n) = M.findWithDefault [] n <$> gets tsConstraints
lookupConstraints _ = return []

cacheType :: (TyperM m) => Expr -> BaseType -> m BaseType
cacheType expr typ = do
  -- liftIO $ putStrLn $+ "cacheType" +|+ S expr +|+ typ
  pure typ <* (modify $ \s -> s { tsExprTypes = M.insert (eID expr) typ (tsExprTypes s) })

cacheTypeM :: (TyperM m) => Expr -> m BaseType -> m BaseType
cacheTypeM expr = join . fmap (cacheType expr)

cacheTypeM_ :: (TyperM m) => Expr -> m BaseType -> m ()
cacheTypeM_ expr = void . cacheTypeM expr

typeof :: (TyperM m) => Expr -> m BaseType
typeof expr = do
  -- liftIO $ putStrLn $+ "typeof" +|+ S expr
  mtyp <- (M.!? (eID expr)) <$> gets tsExprTypes
  -- liftIO $ putStrLn $+ "cached type:" +|+ S mtyp
  case mtyp of
   Nothing -> cacheTypeM expr (constructType expr)
   Just typ -> return typ

constructType :: (TyperM m) => Expr -> m BaseType
constructType (Lit _ lit) = return $ litType lit
constructType (Name _ name) = join $ chaseType <$> lookupTypeOfBinding name
-- constructType (Name _ (Global name typeArgs)) = do
--   schema <- getFunctionType name
--   subst schema typeArgs
constructType (App _ fn args) = do
  fnType <- typeof fn
  argTyps <- mapM typeof args
  retVar <- mkTypeVar
  let fnShape = FunType $ Sig argTyps retVar
  unifyTypes fnType fnShape
  cacheTypeM_ fn (chaseType fnType)
  chaseType retVar
constructType (Let _ name value typ body) = do
  valTyp <- typeof value
  unifyTypes valTyp typ
  isConc <- isConcreteType typ
  -- liftIO $ putStrLn $+ "let binding type:" +|+ typ
  when (not isConc) $ throwError $+ "binding" +|+ name +|+ "contains a polymorphic type"
  withLocal name typ (typeof body)
constructType (Lam _ sig@(Sig argTypes retType) args body) = do
  withLocals (args `zip` argTypes) $ do
    bodyType <- typeof body
    unifyTypes bodyType retType
    chaseType $ FunType sig
constructType (Prop _ recv field) = do
  recvType <- typeof recv
  recordType <- mkTypeVar
  retType <- mkTypeVar
  addConstraint recordType (HasField field retType)
  unifyTypes recordType recvType
  chaseType retType
constructType pre@(Pre _ _) = throwError $+ "unexpected pre-expression found:" +|+ S pre

withLocal :: (TyperM m) => String -> BaseType -> m a -> m a
withLocal name typ action = do
  -- liftIO $ putStrLn $+ "new binding:" +|+ name +|+ ":" +|+ typ
  newLocals <- asks localNames >>= return . M.insert name typ
  local (\e -> e { localNames = newLocals }) action

withLocals :: (TyperM m) => [(String, BaseType)] -> m a -> m a
withLocals namesAndTypes action = do
  -- liftIO $ putStrLn $+ "adding new locals:" +|+ S namesAndTypes
  newLocals <- asks localNames >>= return . mergeLeft (M.fromList namesAndTypes)
  local (\e -> e { localNames = newLocals }) $ do
    -- asks localNames >>= liftIO . putStrLn . tos . ("updated locals:" +|+) . S
    action
  where
    mergeLeft = M.merge M.preserveMissing M.preserveMissing (M.zipWithMaybeMatched (\_ l _ -> Just l))

assertTypeArity :: (TyperErrorM m) => BaseType -> [a] -> [b] -> m ()
assertTypeArity typ expected actual =
  when (length expected /= length actual) $
    throwError $+ "incorrect number of type params for" +|+ typ +:+
    ", expected:" +|+ length expected +:+ ", got:" +|+ length actual

substBase :: (TyperM m) => M.Map String BaseType -> BaseType -> m BaseType
substBase typMap (TypeParam name) =
  case typMap M.!? name of
   Nothing -> return $ TypeParam name
   Just typ -> chaseType typ
substBase typMap (NamedType name types) = do
  (TypeSchema tvs _) <- lookupType name
  when (length tvs /= length types) $
    throwError $+ "incorrect number of type params for" +|+ name +:+
    ", expected:" +|+ length tvs +:+ ", got:" +|+ length types
  NamedType name <$> mapM (substBase typMap) types
substBase typMap (FunType (Sig typParams resType)) =
  FunType <$> (Sig <$> mapM (substBase typMap) typParams <*> substBase typMap resType)
substBase typMap (StructType sname fields fieldTypes) =
  StructType sname fields <$> mapM (substBase typMap) fieldTypes
substBase _ other = return other

assertTypeEq :: (TyperErrorM m) => BaseType -> BaseType -> m ()
assertTypeEq lhs rhs = unless (lhs `typeEquiv` rhs) $ throwError $+ "Type Error:" +|+ lhs +|+ "is not" +|+ rhs

chaseExprTypes :: (TyperM m) => Expr -> m Expr
chaseExprTypes (App eid fn args) =
  App eid <$> chaseExprTypes fn <*> mapM chaseExprTypes args
chaseExprTypes (Lam eid (Sig argTypes retType) params body) =
  Lam eid <$> (Sig <$> mapM chaseType argTypes <*> chaseType retType) <*> pure params <*> chaseExprTypes body
chaseExprTypes (Let eid name value typ body) =
  Let eid name <$> chaseExprTypes value <*> chaseType typ <*> chaseExprTypes body
chaseExprTypes (Prop eid recv pname) =
  Prop eid <$> chaseExprTypes recv <*> pure pname
chaseExprTypes local@(Name _ (Local _)) = return local
chaseExprTypes (Name eid (Global name types)) =
  Name eid . Global name <$> mapM chaseType types
chaseExprTypes lit@(Lit _ _) = return lit
chaseExprTypes pre@(Pre _ _) = throwError $+ "unexpected pre-expression found:" +|+ S pre

chaseType :: (TyperM m) => BaseType -> m BaseType
chaseType (TypeVar n) = gets tsTypeVars >>= maybe (return $ TypeVar n) chaseType . (M.!? n)
chaseType (NamedType n args) = do
  underlyingSchema <- lookupType n
  substRes <- join $ subst underlyingSchema <$> mapM chaseType args
  chaseType substRes
chaseType (FunType (Sig typs ret)) = FunType <$> (Sig <$> mapM chaseType typs <*> chaseType ret)
chaseType (StructType sname fields typs) = StructType sname fields <$> mapM chaseType typs
chaseType typ = return typ

isConcreteType :: (TyperM m) => BaseType -> m Bool
isConcreteType tv@(TypeVar _) = do
  chased <- chaseType tv
  case chased of
   (TypeVar _) -> return False
   typ -> isConcreteType typ
isConcreteType (NamedType _ args) = all id <$> mapM isConcreteType args
isConcreteType (FunType (Sig argTypes retType)) = do
  argsConcrete <- mapM isConcreteType argTypes
  retConcrete <- isConcreteType retType
  return (all id argsConcrete && retConcrete)
isConcreteType (StructType _ _ typs) = all id <$> mapM isConcreteType typs
isConcreteType _ = return True

subst :: forall m. (TyperM m) => TypeSchema -> [BaseType] -> m BaseType
subst (TypeSchema typParams baseTyp) typs = do
  -- liftIO $ putStrLn $+ "subst (TypeSchema" +|+ S typParams +|+ baseTyp +:+ ")" +|+ S typs
  -- liftIO $ putStrLn $+ S typParams +|+ "`zip`" +|+ S typs +|+ ";=>" +|+ S (typParams `zip` typs)
  res <- substBase typMap baseTyp
  -- liftIO $ putStrLn $+ "subst result:" +|+ res
  return res
  where
    typMap = M.fromList $ typParams `zip` typs

varCycleCheck :: (TyperM m) => Int -> BaseType -> m ()
varCycleCheck idx typ = do
  chased <- chaseType typ
  case chased of
   FunType (Sig typs ret) -> do
     mapM_ (varCycleCheck idx) typs
     varCycleCheck idx ret
   TypeVar n -> when (n == idx) $ throwError "unification cycle detected!"
   _ -> return ()

setTypeVar :: (TyperM m) => Int -> BaseType -> m ()
setTypeVar idx typ = do
  varCycleCheck idx typ
  -- liftIO $ putStrLn $+ "setting type var" +|+ idx +|+ "to" +|+ typ
  modify $ \s -> s { tsTypeVars = M.insert idx typ (tsTypeVars s) }

addConstraint :: (TyperM m) => BaseType -> Constraint -> m ()
addConstraint tv@(TypeVar n) constr = do
  old <- lookupConstraints tv
  newConstrs <- mergeConstraints constr old
  modify $ \s -> s { tsConstraints = M.insert n newConstrs (tsConstraints s) }
addConstraint typ _ = throwError $+ "cannot add constraints to concrete type" +|+ typ

mergeConstraints :: (TyperM m) => Constraint -> [Constraint] -> m [Constraint]
mergeConstraints new@(HasField fname ftyp) existing = do
  case find (fieldMatch fname) existing of
   Nothing -> return (new:existing)
   Just (HasField _ otherType) -> do unifyTypes ftyp otherType
                                     return existing
  where
    fieldMatch f (HasField ((==f) -> res) _) = res
    -- fieldMatch _ _ = False

applyConstraint :: forall m. (TyperM m) => Constraint -> BaseType -> m ()
applyConstraint cns typ = join $ apply' True cns <$> chaseType typ
  where
    apply' :: Bool -> Constraint -> BaseType -> m ()
    apply' _ cns tv@(TypeVar _) = addConstraint tv cns
    apply' _ (HasField fname ftyp) (StructType _ fields ftyps) = do
      case fname `elemIndex` fields of
       Nothing -> throwError $+ "type" +|+ typ +|+ "does not have field" +|+ fname
       Just n -> unifyTypes ftyp (ftyps !! n)
    apply' True cns@(HasField _ _) (NamedType n _) = join $ apply' False cns <$> (underlyingType <$> lookupType n)
    apply' _ (HasField _ _) typ = throwError $+ "type" +|+ typ +|+ "does not have fields"
    -- apply' _ (HasTypeArgs ctyps) t@(NamedType _ ntyps) = do
    --   assertTypeArity t ctyps ntyps
    --   mapM_ (uncurry unifyTypes) (ctyps `zip` ntyps)
    -- apply' _ (HasTypeArgs _) typ = throwError $+ "type" +|+ typ +|+ "does not take parameters"

validateConstraints :: forall m. (TyperM m) => [Constraint] -> BaseType -> m ()
validateConstraints constraints typ = mapM_ (`applyConstraint` typ) constraints

unifyTypes :: (TyperM m) => BaseType -> BaseType -> m ()
unifyTypes t1 t2 = do
  -- liftIO $ putStrLn $+ "unify types:" +|+ t1 +|+ t2
  join $ aux <$> chaseType t1 <*> chaseType t2
  where
    aux tv@(TypeVar n) t2 = do
      constrs <- lookupConstraints tv
      validateConstraints constrs t2
      setTypeVar n t2
    aux t1 tv@(TypeVar n) = do
      constrs <- lookupConstraints tv
      validateConstraints constrs t1
      setTypeVar n t1
    aux (FunType (Sig typsLHS retLHS)) (FunType (Sig typsRHS retRHS)) = do
      when (length typsLHS /= length typsRHS) $
        throwError $+ "mismatched param counts - lhs:" +|+ length typsLHS +:+ ", rhs:" +|+ length typsRHS
      mapM_ (uncurry unifyTypes) $ typsLHS `zip` typsRHS
      unifyTypes retLHS retRHS
    aux t1 t2 = assertTypeEq t1 t2
