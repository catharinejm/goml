package main

import (
	"fmt"
)

func _DOLLAR__specialized2816(f_2815 func(a int) int, x_2815 int) int {
	return f_2815(x_2815)
}
func _STAR_(x, y int) int {
	return x * y
}
func liftyfun() {
	var y_2821 int = 10
	var x_2820 int = y_2821
	var z_2819 int = 42
	_STAR_(z_2819, x_2820)
}
func liftyfun2() int {
	var y_2824 int = 10
	var x_2823 int = y_2824
	var z_2822 int = 42
	return _STAR_(z_2822, x_2823)
}
func testfun() {
	_DOLLAR__specialized2816(times2, 5)
}
func times2(x_2807 int) int {
	return _STAR_(x_2807, 2)
}

func main() {
	fmt.Println(liftyfun2())
}
